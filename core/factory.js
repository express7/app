const debug = require('debug')('app')

module.exports = options => {
  global.$ = { ...global.$, ...options }

  const express = require('express')
  const createError = require('http-errors')
  const _ = require('app/helpers')
  const Config = require('app/config')
  const { routes } = options

  // app
  $.app = express()

  // load events
  try {
    for (const v of Object.values($.events)) v($.app)
  } catch (e) {
    debug(e)
  }

  // load middlewares
  for (const item of Config.get('middleware', [])) {
    try {
      const [middleware, name] = typeof item === 'function' ? [item, item.name] : [require(item), item]

      const [key] = _.kebabCase(name.split('/').reverse())

      $.app.use(middleware(Config.get(key), $.app))
      debug('middleware loaded:', name)
    } catch (e) {
      debug('middleware failed:', e)
    }
  }

  return $.app
    // static
    .use(express.static(_.publicPath()))

    // request adjusment
    .use((req, res, next) => {
      // fix params
      for (const p of [req.params, req.query, req.body]) {
        for (const k of Object.keys({ ...p })) p[_.camelCase(k)] = p[k]
      }

      // all queries
      req.all = () => ({
        ..._.camelCase(_.pickBy({ ...req.body, ...req.query, ...req.params }, _.identify)),
        ..._.pickBy({ ...req.body, ...req.query, ...req.params }, _.identify)
      })
      
      // sse response
      res.sse = (json, { id = +(req.get('last-event-id') || 0) + 1 } = {}) => {
        const { retry, event } = req.all()

        res.writeHead(200, {
          'Content-Type': 'text/event-stream',
          'Cache-Control': 'no-cache',
          'Connection': 'keep-alive',
          'Access-Control-Allow-Origin': '*'
        })

        res.write('\n')
        if (retry) res.write(`retry: ${retry}\n`)
        if (event) res.write(`event: ${event}\n`)
        res.write(`id: ${id}\n`)
        res.write(`data: ${JSON.stringify(json)}\n\n`)

        return res.end()
      }

      next()
    })

    // routes
    .use(routes.path || '/', routes)
    
    // catch 404 and forward to error handler
    .use((req, res, next) => next(createError(404)))

    // error handler
    .use((err, req, res, next) => {
      debug(req._startTime, '-> err', err)
      
      res.locals.message = err.message
      res.locals.error = req.app.get('env') === 'development' ? err : {}

      // render the error page
      res.status(err.status || 500)

      // res.render('error')
      debug(req._startTime, 'err ->', err.status, err)
      return res.send(err)
    })
}
