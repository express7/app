const debug = require('debug')('app')
const globals = require('app/helpers/globals')
// const flinks = require('app/helpers/filelink')
const createApp = require('./factory')

// create app
try {
  // commonjs
  const options = {
    helpers: { ...requireDir('root/helpers'), ...requireDir('root/app/helpers') },
    config: requireDir('root/config'),
    providers: requireDir('root/app/providers'),
    models: requireDir('root/app/models'),
    middlewares: requireDir('root/app/middlewares'),
    events: requireDir('root/app/events'),
    routes: require('root/app/routes')
  }

  module.exports = createApp(options)
} catch (e) {
  debug('esm mode', e)

  // esm
  module.exports = createApp
}
