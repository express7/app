const debug = require('debug')('app:provider')

const isClass = func => {
  return typeof func === 'function' 
    && /^class\s/.test(Function.prototype.toString.call(func))
}

global.$ = { providers: {}, ...global.$ }

module.exports = (name, options = {}) => {
  const Config = require('app/config')
  const provider = name
  const drivers = { app: $.app, ...$.providers[name] }
  const defDriver = Config.get(`${provider}.default`, Config.get(`${provider}.connection`))

  // default loader function
  const load = (name, options) => {
    if (isClass(name)) return new name(options)
    if (typeof name === 'function') return name(options)
    if (typeof name !== 'string') return name
  
    try {
      const mod = global.require(`root/app/providers/${provider}/${name}`, `app/${provider}/lib/${name}`, `app/${provider}/${name}`, name)
      return isClass(mod) ? new mod(options) : mod(options)
    } catch (e) {
      const message = `App: run\n$ npm install ${name}`;
      throw new Error(`${message}\n${e.message}`);
    }
  }  

  // use function
  const use = (name = defDriver) => {
    if (typeof name !== 'string') return loader(name, name)
    if (drivers[name]) return drivers[name]

    const { driver, ...opts } = Config.get(`${provider}.${name}`, {})
    drivers[name] = loader(driver, { ...Config.get(provider, {}), ...opts, ...options })

    return drivers[name]
  }

  const loader = options.loader || load

  const driver = use(defDriver)

  return require('app/core/proxy')([{ use, driver: use, connection: use, drivers }, driver])
}
