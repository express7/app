const debug = require('debug')('app:proxy')

const getHandler = (prop, targets) => {
  for (let target of targets) {
    target = (typeof target === 'function' && target.name === '') ? target() : target
    if (target && target[prop]) return target
  }
}

module.exports = (targets, options) => {
  return new Proxy(targets.pop(), {
    apply (target, thisArg, args) {
      return target(...args)
    },

    get (target, prop, receiver) {
      const handler = getHandler(prop, targets) || target
      const value = Reflect.get(handler, prop, receiver)
      return typeof value === 'function' ? value.bind(target) : value

      // return typeof handler[prop] === 'function'
      //   ? (...args) => handler[prop].apply(handler, args)
      //   : handler[prop]
    },

    ...options
  })
}