'use strict'

const debug = require('debug')('app:router')
const _ = require('app/helpers')
const extractRoute = require('./extract-route')

module.exports = (router, options = {}) => {
  let { routes = {}, deep = 0, depth = 20, rpath: r = '' } = options

  // if too deep
  if (deep++ > depth) return routes

  const { _router, handle, route, stack = [] } = router

  for (const layer of stack) {
    const [vpath, mtds = {}] = _.flatten(Object.entries(extractRoute(layer) || {}))

    const [method = {}, v] = _.flatten(Object.entries(mtds))
    const methods = method === '_all' ? ['get', 'post'] : [_.castType(method)]

    const rpath = !methods[0] ? path.posix.join('/', r, vpath) : r

    if (vpath && methods[0]) {
      const url = path.posix.join('/', rpath, vpath)
      for (const method of methods) routes[url] = { ...routes[url], [method]: { ...v, rpath } }
    }

    const { _router, handle, route } = layer

    if (_router) getRoutes(_router, { ...options, routes, rpath })
    if (handle) getRoutes(handle, { ...options, routes, rpath })
    if (route) getRoutes(route, { ...options, routes, rpath })
  }

  if (_router) getRoutes(_router, { ...options, routes })
  if (handle) getRoutes(handle, { ...options, routes })
  if (route) getRoutes(route, { ...options, routes })

  return routes
}
