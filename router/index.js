'use strict'

const debug = require('debug')('app:router')
const path = require('node:path')
const callsites = require('callsites')
const { Router } = require('express')
const getRoutes = require('./getroutes')
const catcherPatch = require('./catcher-patch')

debug('constructor')

// plugins
Router.dir = function (prefix = '', dirpath = path.dirname(callsites()[1].getFileName())) {
  for (const [filename, router] of Object.entries(requireDir(dirpath, { exclude: /index\.js$/ }))) {
    // skip if not router instance
    if (Object.getPrototypeOf(router) !== Router) continue

    // path
    router.path = path.posix.join('/', router.path || filename)
    // // if kebab path 
    // const kpath = kebabUrl(router.path)
    // router.path = kpath === router.path ? router.path : `(${router.path}|${kpath})?`

    // // url kebabcase
    // router.stack.map(layer => extractRoute(layer))
    //   .filter(s => !!s) // remove undefined
    //   .forEach((route, i, arr) => {
    //     const [[vpath, methods]] = Object.entries(route)
    //     const kpath = kebabUrl(vpath)
    //     if (!_.flatten(arr.map(c => Object.keys(c))).includes(kpath)) {
    //       const [[method, v]] = Object.entries(methods)

    //       // add kebab route
    //       router[_.lowerCase(method)](kpath, v.handles)
    //     }
    //   })

    // use route    
    this.use(router.path, router)
  }

  return this
}

module.exports = require('app/core/proxy')([
  { Router: catcherPatch, getRoutes },
  require('app/middleware'),
  catcherPatch
])
