'use strict'

const debug = require('debug')('app:router')
const methods = require('methods')
const { Router } = require('express')
const createError = require('http-errors')

// monkey patch methods
module.exports = (...args) => {
  const router = Router(...args)
  const [options] = args
  if (options.path) router.path = options.path

  for (const method of methods){
    const original = router[method]

    const rep = {
      [method]: (path, ...callbacks) => {
        const controller = callbacks[callbacks.length - 1]

        callbacks[callbacks.length - 1] = async (...args) => {
          const [req, res, next] = args

          try {
            debug('->', req.originalUrl, req.all())

            const response = await controller.apply(router, args)

            debug(req.originalUrl, '->')
            return response
          } catch (err) {
            debug(req.originalUrl, '-x', err)
            return next(createError(500, err))
          }
        }

        return original.apply(router, [path, ...callbacks])
      }
    }

    router[method] = rep[method]
  }

  return router
}
