'use strict'

const debug = require('debug')('app:router')
const _ = require('app/helpers')

debug('constructor')

const kebabUrl = path => path?.replace(/(.*?)[\?.\/]|(:.*)|\w+/gi, m => {
  return m?.startsWith(':') ? m : m.replace(/\w+/gi, m => {
    return _.kebabCase(_.words(m)).map(c => {
      return _.isNumber(_.castType(c)) ? c : '-' + c
    }).join('').replace(/^\-/gi, '')
  })
})

module.exports = (layer, options = {}) => {
  const { handle = {}, route = {}, regexp } = layer

  // url path
  const path = (layer.path || handle.path || route.path || regexp.toString().replace(/\W+/gi, '/').replace(/\/i$/gi, ''))
    .replace(/\(\/.*\|(.*?)\)/gi, '$1').replace(/\)/gi, '') || undefined // remove multiple path like (/signout|/logout|/keluar).:format?

  if (!path) return

  // method
  const [method] = Object.keys(layer.methods || handle.methods || route.methods || [])

  const handles = route.stack ? route.stack.map(s => s.handle) : undefined

  return ({ [path]: { [method]: { path, ...options, handles } } })
}
