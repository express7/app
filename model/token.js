const Model = require('.')

/**
 * @schema User
 * required:
 *   - username
 *   - email
 *   - password
 * properties:
 *   id:
 *     type: integer
 *     format: int64
 *   username:
 *     type: string
 *   email:
 *     type: string
 *   password:
 *     type: string
 */
class Token extends Model {
  static get props () {
    return ({
      id: 'increments',
      userId: ['integer'],
      token: ['text']
    })
  }

  static get hidden () {
    return ['createdAt', 'updatedAt', 'deletedAt']
  }
}

module.exports = require('app/core/proxy')([global.$?.token, Token])
