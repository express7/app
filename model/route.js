const debug = require('debug')('app:route:model')
const _ = require('app/helpers')
const Config = require('app/config')
const Models = require('./models')
// const Cache = require('../cache')
// const hash = require('object-hash')
const { Router, check, validate, version } = require('app/router')
const createError = require('http-errors')

const v = version({ default: '1.0.0' })
// const minutes = Config.get('cache.minutes', 10)

function modeling (req, { schema, ...options }) {
  const { model } = req.all()

  // find model
  const { [_.pascalCase(_.singularize(model))]: Model } = Models

  if (Model) return Model
  
  if (options.includeSchema) return ({
    schema,
    table: model,
    connection: Config.get('database.connection'),
    ...require('app/database/crud'), 
    ...require('app/database/query')
  })
}

module.exports = options => Router({ mergeParams: true, ...options })
  // Create a new item
  .post('/:model.:format?', v('1.0.0'),
    // validation
    check('model').not().isEmpty().trim(),

    // control
    async (req, res, next) => {
      try {
        if (req.body.constructor === Object &&
          Object.keys(req.body).length === 0) next(createError(400, 'Please provide all required field'))

        // define model
        const Model = options.customModel
          ? await options.customModel(req, options)
          : modeling(req, { ...options, ...req.query })

        // create model from request
        const model = new Model(req.body)
        const result = await model.save(req.query)

        if (!result) return next(createError(400))

        req.app.emit(`${Model.name}:create`, result)

        // send response
        return res.status(201).json(result)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  // Retrieve all items
  .get('/:model.:format?', v('1.0.0'),
    // validation
    check('model').not().isEmpty().trim(),

    // control
    async (req, res, next) => {
      try {
        const {
          format,
          schema,
          column,
          columns = column,
          limit = +Config.get('database.limit', 10),
          items = +limit,
          page = 1,
          start = (+page - 1) * +items,
          offset = +start,
          end = +offset + +items,
          order,
          sort,
          ...params
        } = req.all() 

        // define model
        const Model = options.customModel
          ? await options.customModel(req, options)
          : modeling(req, { ...options, ...req.query })

        // check ordering
        const orders = sort
          ? (Array.isArray(sort) ? sort : [sort]).map((column, i) => Object.assign({ column }, { order: order[i] }))
          : undefined

        // get rows
        const result = await Model.find(params, { schema, columns, offset, limit: +items, orders })

        // get total rows for response header
        // const key = hash({ $method: 'total', name: Model.name, ...Model, ...params })
        // const total = await Cache.remember(key, minutes, async () => await Model.count(params, { schema, offset, limit, orders }))
        const total = await Model.count(params, { schema, offset, limit: 1, orders })
        const pages = Math.floor(Number(total) / +items) + 1

        // set as headers
        if (res) res.set(Object.entries({ total, pages, items }).reduce((a, [k, v]) => ({ ...a, [`X-${_.capitalize(k)}`]:v }), {}))

        return res.json(result)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  // Retrieve a single item with id
  .get('/:model/:id.:format?', v('1.0.0'),
    // validation
    check('model').not().isEmpty().trim(),
    check('id').not().isEmpty().trim(),

    // control
    async (req, res, next) => {
      try {
        const { id } = req.params

        // define model
        const Model = options.customModel
          ? await options.customModel(req, options)
          : modeling(req, { ...options, ...req.query })

        const [key = Object.keys(Model.props)[0]] = Model.keys

        // get data
        const [result] = await Model.find({ [key]: id })

        if (!result) return next(createError(404))

        return res.json(result)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  // Update a item with id
  .put('/:model/:id.:format?', v('1.0.0'),
    // validation
    check('model').not().isEmpty().trim(),
    check('id').not().isEmpty().trim(),

    // control
    async (req, res, next) => {
      try {
        const { id } = req.params

        // define model
        const Model = options.customModel
          ? await options.customModel(req, options)
          : modeling(req, { ...options, ...req.query })

        const [key = Object.keys(Model.props)[0]] = Model.keys
        const model = new Model({ [key]: id, ...req.body })

        // save data
        const result = await model.save()

        req.app.emit(`${Model.name}:update`, result)

        // send response
        if (result) return res.json(result)
        
        // if empty result but successful
        return res.sendStatus(204)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  // Delete a item with id
  .delete('/:model/:id.:format?', v('1.0.0'),
    // validation
    // check('model').not().isEmpty().trim(),
    // check('id').not().isEmpty().trim(),

    // control
    async (req, res, next) => {
      try {
        const { id } = req.params

        // define model
        const Model = options.customModel
          ? await options.customModel(req, options)
          : modeling(req, { ...options, ...req.query })

        const [key = Object.keys(Model.props)[0]] = Model.keys
        const model = new Model({ [key]: id, ...req.body })

        // delete data
        const result = await model.remove()

        req.app.emit(`${Model.name}:delete`, result)

        // send response
        if (result) return res.json(result)
        
        // if empty result but successful
        return res.sendStatus(204)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )
