const Model = require('.')

/**
 * @schema User
 * required:
 *   - username
 *   - email
 *   - password
 * properties:
 *   id:
 *     type: integer
 *     format: int64
 *   username:
 *     type: string
 *   email:
 *     type: string
 *   password:
 *     type: string
 */

class User extends Model {
  static get props () {
    return ({
      id: 'increments',
      username: { string: 80 },
      email: [String],
      password: ['text'],
      status: [String],
      roles: [{ specificType: 'text[]' }],
      loginAt: [{ timestamp: { precision: 6, useTz: true } }]
    })
  }

  static get hidden () {
    return ['password', 'updatedAt', 'deletedAt']
  }

  // tokens () {
  //   return this.query().nest(require('../models/token'), 'id', 'userId')
  // }
}

module.exports = require('app/core/proxy')([global.$?.user, User])
