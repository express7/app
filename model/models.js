const _ = require('app/helpers')
const exclude = /(index|models|route)\.js$/

let models = {}

const { dependencies = {} } = require('../../../package.json')
for (const [k, v] of Object.entries(dependencies).filter(([k, v]) => v.endsWith('.git'))) {
  try {
    models = { ...models, ...requireDir(`${k}/model`, { exclude }) }
  } catch (err) {}
}

models = Object.entries({ ...models, ...requireDir('app-root/models', { exclude }) })
  .reduce((a, [k,v]) => ({ ...a, [_.pascalCase(_.singularize(k))]: v.table ? v : null }), {})

module.exports = _.pickBy(models, _.identity)
