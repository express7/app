const debug = require('debug')('app:model')
const _ = require('app/helpers')
const Config = require('app/config')
const Database = require('app/database')
const { deepParseJson } = require('deep-parse-json')

// const omit = (obj, keys) => {
  // debug('-> omit keys:', keys, obj)
  
  // const result = Object.entries(obj)
    // .filter(([k]) => !keys.includes(k))
    // .reduce((a, c, i, ar) => ({ ...a, [ar[i][0]]: ar[i][1] }), {})

  // debug('omit ->', result, obj)
  // return result
// }

class Model {
  // props
  static get props () {
    return ({
      id: 'increments',
      createdAt: [{ timestamp: { useTz: true } }],
      updatedAt: [{ timestamp: { useTz: true } }]
      // field1: ['float', { default: null }, { fieldName: name => name }],
    })
  }

  static get columns () {
    if (!this._columns) {
      this._columns = Object.entries(this.props).reduce((a, [k, v]) => {
        const { fieldName = _.snakeCase } = (Array.isArray(v) ? v : [v]).find((r = {}) => r.fieldName) || {}
        return ({ ...a, [k]: fieldName(k) })
      }, {})

      Object.defineProperty(this, '_columns', { enumerable: false })
    }

    return this._columns
  }

  static get columnsModel () {
    if (!this._columnsModel) {
      this._columnsModel = Object.entries(this.columns)
        .reduce((a, [k, v]) => ({ ...a, [v]: k, [k]: k }), {})

      Object.defineProperty(this, '_columnsModel', { enumerable: false })
    }

    return this._columnsModel
  }

  static get columnsDB () {
    if (!this._columnsDB) {
      this._columnsDB = Object.entries(this.columns)
      .reduce((a, [k, v]) => ({ ...a, [k]: v, [v]: v }), {})

      Object.defineProperty(this, '_columnsDB', { enumerable: false })
    }

    return this._columnsDB
  }

  static get columnsDefault () {
    if (!this._columnsDefault) {
      this._columnsDefault = Object.entries(this.props)
        .reduce((a, [k, v]) => {
          const [field, ...chains] = Array.isArray(v) ? v : [v]
          const { default: val } = chains.find(c => c.default) || {}

          return val ? { ...a, [k]: val } : a
        }, {})

      Object.defineProperty(this, '_columnsDefault', { enumerable: false })
    }

    return this._columnsDefault
  }

  // database
  static get connection () {
    if (!this._connection) {
      this._connection = Config.get('database.connection')

      Object.defineProperty(this, '_connection', { enumerable: false })
    }

    return this._connection
  }

  static set connection (value) {
    this._connection = value
  }

  static get schema () {
    if (this._schema) return _.tableize(this._schema)
  }

  static set schema (value) {
    this._schema = _.tableize(value)

    Object.defineProperty(this, '_schema', { enumerable: false })
  }

  static get table () {
    if (!this._table) {
      this._table = _.tableize(this.name)
    
      Object.defineProperty(this, '_table', { enumerable: false })
    }

    return _.tableize(this._table)
  }

  static set table (value) {
    this._table = _.tableize(value)
  }

  static get keys () {
    if (!this._keys) {
      this._keys = ['id']

      Object.defineProperty(this, '_keys', { enumerable: false })
    }

    return this._keys
  }

  static set keys (value) {
    this._keys = value
  }

  // extended
  static get primaryKeys () {
    return Object.values(_.pick(this.columns, this.keys))
  }

  static get hidden () {
    if (!this._hidden) {
      this._hidden = []

      Object.defineProperty(this, '_hidden', { enumerable: false })
    }

    return this._hidden
  }

  static set hidden (value) {
    this._hidden = value
  }

  // ddl
  static async createTable () {
    const exists = await Database.driver(this.connection).schema.hasTable(this.table)

    if (exists) return

    const schema = Database.driver(this.connection).schema
    if (this.schema) schema.createSchemaIfNotExists(this.schema).withSchema(this.schema)

    return schema.createTable(this.table, (table) => {
      for (const [name, prop] of Object.entries(this.props)) {
        const [main, ...chains] = Array.isArray(prop) ? prop : [prop, 'notNullable']
        const actions = [action(main), ...chains.map(c => action(c)).filter(c => !c.fieldName)]

        let column = table
        for (const action of actions) {
          const [key] = Object.keys(action)

          column = column[key](this.columns[name], action[key])
        }
      }

      // add primary
      table.primary(this.primaryKeys)

      // add timestamps columns
      table.timestamps(true, true)
    })
  }

  // sql
  static query (options = {}) {
    const knex = options.trx || options.connection || Database.driver(this.connection)

    const query = knex(this.table)

    // function
    if (typeof this.table !== 'string') query.fromFn(this.table)

    // schema
    if (this.schema) query.withSchema(this.schema)
    
    // limit
    const limit = Config.get('database.limit')
    if (limit) query.limit(limit)

    return query
  }

  /**
   * Saves the properties currently set on the model.
   *
   * @param {Object} opts Options for saving.
   * @return {*} An updated copy of the model.
   */
  constructor (data = {}, options = {}) {
    debug('-> new', this.constructor.name, 'options:', options, data)

    this.$properties = { ...this.constructor.columnsDefault }

    // fill data
    const c = this.constructor.columnsModel
    for (const [k, v] of Object.entries(data)) {
      const prop = c[k] ? this.$properties : this
      prop[c[k] || k] = v
    }

    debug('new ->', this.constructor.name, this)
    // return proxy instead instance
    return require('app/core/proxy')([this.$properties, this], {
      ownKeys (target) {
        return Object.keys(target.constructor.props)
      },

      set (target, prop, value) {
        const c = target.constructor.columnsModel
        const t = c[prop] ? target.$properties : target
        // t[c[prop] || prop] = value    
        return Reflect.set(t, c[prop] || prop, value )
      },
    
      deleteProperty (target, prop) {
        Reflect.deleteProperty(target.$properties, prop) 
        Reflect.deleteProperty(target, prop)
      }
    })
  }

  /**
   * Saves the properties currently set on the model.
   *
   * @param {Object} opts Options for saving.
   * @return {*} An updated copy of the model.
   */
  async save (options = {}) {
    debug('-> save', this.constructor.name, 'options:', options, this.$properties)

    const opt = {
      ...options,
      table: this.constructor.table,
      keys: this.constructor.primaryKeys
    }

    // fill data
    const c = this.constructor.columnsDB
    const data = Object.entries(this.$properties).reduce((a, [k, v]) => ({ ...a, [c[k]]: v }), {})

    // save properties
    const [props] = await this.constructor.save(data, opt)

    debug('save ->', this.constructor.name, props)
    return this
  }

  /**
   * Saves the properties currently set on the model.
   *
   * @param {Object} opts Options for saving.
   * @return {*} An updated copy of the model.
   */
  async remove (options = {}) {
    const opt = {
      ...options,
      table: this.constructor.table,
      keys: this.constructor.primaryKeys
    }

    // fill data
    const c = this.constructor.columnsDB
    const data = Object.entries(this.$properties).reduce((a, [k, v]) => ({ ...a, [c[k]]: v }), {})

    //  delete item
    const [result] = await this.constructor.remove(data, opt)

    return result
  }

  query (options) {
    return this.constructor.query(options)
  }

  toJSON (options = {}) {
    debug('-> toJSON options:', options, this)

    const { $properties, ...props } = deepParseJson(this)
    const json = { ...props, ...$properties }

    debug('toJSON ->', json)
    return json
  }
}

function action (prop) {
  const key = typeof prop === 'string'
    ? prop
    : typeof prop === 'function'
      ? prop.name.toLowerCase()
      : Object.keys(prop)[0]

  const { [key]: name = key } = { number: 'float' }

  return ({
    [name]: key.indexOf('increment') < 0 
      ? prop[key] 
      : { primaryKey: false }
  })
}

Object.assign(Model, require('app/database/crud'), require('app/database/query'))

Model._find = Model.find

Object.assign(Model, {
  async find (filter = {}, options = {}) {
    debug('-> find', this.name, 'options:', options, filter)

    const items = await this._find(filter, options) || []

    // map as new items
    const result = items.map(item => {
      for (const key of Object.keys(item)) {
        try {
          item[key] = JSON.parse(item[key])
        } catch (err) {}
      }

      return new this(item)
    })

    debug('find ->', this.name, result)
    return result
  }
})

module.exports = Model
