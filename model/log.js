const path = require('node:path')
const _ = require('app/helpers')
const Model = require('.')

class Log extends Model {
  static get props () {
    return ({
      timeStamp: { timestamp: { useTz: true } },
      ipAddress: [String],
      status: [String],
      userId: [String],
      service: [String],
      // server: [String],
      // serverIp: [String],
      // serverPort: [String],
      protocol: [String],
      method: [String],
      resource: [String],
      // uriStem: [String],
      // uriQuery: [String],
      byteSent: [String],
      byteRecv: [String],
      duration: [String]
      // protocolVersion: [String],
      // host: [String],
      // userAgent: [String],
      // cookie: [String],
      // referrer: [String]
    })
  }

  static get connection () {
    return ({
      name: 'logdb',
      client: require('app/database/duckdb'),
      connection: { filename: ':memory:' }
    })
  }

  static get table () {
    const cols = Object.entries(this.props).reduce((a, [k, v]) => ({ ...a, [k]: 'varchar' }), {})
    return { raw: `read_csv('${path.join(_.tmpPath(), 'log', 'app-*')}', header=false, ignore_errors=true, sep='\\t', columns=${JSON.stringify(cols)})` }
  }
}

module.exports = require('app/core/proxy')([global.$?.log, Log])
