const Model = require('.')

class Role extends Model {
  static get props () {
    return ({
      id: 'increments',
      name: { string: 80 },
      resources: [String]
    })
  }
}

module.exports = require('app/core/proxy')([global.$?.role, Role])
