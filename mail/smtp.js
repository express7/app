'use strict'

const debug = require('debug')('app:mail')
const nodemailer = require('nodemailer')

debug('constructor')

module.exports = (options) => {
  // create reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport(options)

  return {
    send: async ({ to, subject, text, html }) => {
      const info = await transporter.sendMail({
        from: `"Admin" <${options.auth.user}>`,
        to, subject, text, html
      })

      return info
    }
  }
}
