const debug = require('debug')('app:helpers:util')
const path = require('node:path')
const _ = require('lodash')

debug('constructor')

_.mixin({ pascalCase: _.flow(_.camelCase, _.upperFirst) })

const isClass = func => {
  return typeof func === 'function' 
    && /^class\s/.test(Function.prototype.toString.call(func))
}

exports.isClass = isClass

exports.paginate = (arrays, req, res) => {
  if (!Array.isArray(arrays)) return arrays

  const Config = require('app/config')

  const {
    limit = +Config.get('database.limit', 10),
    items = +limit,
    page = 1,
    start = (+page - 1) * +items,
    offset = +start,
    end = +offset + +items,
  } = req ? req.all() : {}

  const { length: total } = arrays
  const pages = Math.floor(total / +items) + 1
  const rows = arrays.slice(offset, end)
  rows.total = +total
  rows.pages = +pages
  rows.items = +items

  try {
    // set as headers
    if (res) res.set(Object.entries({ total, pages, items }).reduce((a, [k, v]) => ({ ...a, [`X-${_.capitalize(k)}`]:v }), {}))
  } catch (err) {
    debug(err)
  }

  return rows
}

exports.castType = value => {
  try {
    if (typeof value === 'string') return (new Function(`return ${value}`))()
  } catch (err) {
    return value
  }

  if (Array.isArray(value)) return value.map(v => {
    try { 
      return (new Function(`return ${v}`))() 
    } catch (err) {
      return v
    }
  })

  return Object.entries(value).reduce((a, [k, v]) => {
    try {
      a[k] = (new Function(`return ${v}`))()
    } catch (err) {
      a[k] = v
    }

    return a
  }, {})
}
