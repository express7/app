const debug = require('debug')('app:filelink')
const fs = require('node:fs')
const path = require('node:path')

const rootPath = process.mainModule.path

// create symbolic links
let links = [
  ['', 'root'], // root link
  ['app', 'app-root'], // app link
  ['app/middlewares', 'middlewares'], // app middlewares
  ['app/models', 'models'], // app models
  ['app/commands', 'commands'] // app commands
]

links = links.map(c => [path.join(rootPath, c[0]), path.join(rootPath, 'node_modules', c[1])])

for (const [tgpath, lnpath] of links) {
  try {
    debug('symlink', tgpath, lnpath)
    fs.symlinkSync(tgpath, lnpath)
  } catch (e) {
    debug(e)
  }
}
