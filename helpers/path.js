const debug = require('debug')('app:helpers:path')
const { join } = require('node:path')

const rootPath = process.mainModule.path

// helpers
module.exports = {
  appPath: () => join(rootPath, 'app'),
  publicPath: () => join(rootPath, 'public'),
  rootPath: () => rootPath,
  tmpPath: () => join(rootPath, 'tmp'),
  viewPath: () => join(appPath, 'views'),
  require: global.require,
  requireDir: global.requireDir,
  rootRequire: global.rootRequire,
  dirRequire: global.requireDir
}
