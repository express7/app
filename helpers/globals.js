const debug = require('debug')('app:globals')

debug('set $')
global.$ = { ...global.$ }

// set require with faallback
const requireReal = require
const requireFallback = (...args) => {
  debug('require', args)

  const [modul, ...moduls] = args

  try {
    const result = typeof modul !== 'string' ? modul : requireReal(modul)

    debug('require', result)
    return result
  } catch (e) {
    debug(e)
    return moduls.length > 0 ? requireFallback(...moduls) : requireReal(modul)
  }
}

debug('set require')
global.require = requireFallback


// set require directory
const requireDirectory = require('require-directory')

debug('set requireDir')
global.requireDir = (path, options = {}) => {
  debug('requireDir', path)
  const { recurse = false, flat = false, separator = '' } = options

  try {
    const result = requireDirectory(
      { require, filename: `${process.mainModule.path}/node_modules/package.json` },
      path,
      { recurse, include: /s$/, ...options }
    )

    debug('requireDir', result)
    return result
  } catch (e) {
    debug(path, e)

    return {}
  }
}

// set fetch
debug('set fetch')
global.fetch = global.fetch || require('fetch', 'got', 'axios', undefined)


// define require from app root directory

const rootPath = process.mainModule.path

const rootRequire = name => global.require(join(rootPath, name))
global.rootRequire = rootRequire

global.dirRequire = global.requireDir
