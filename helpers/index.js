const debug = require('debug')('app:helpers')
const lodash = require('lodash')
const inflection = require('inflection')

debug('constructor')

// object enabled cases
const cases = [
  ...Object.keys(inflection).map(c => [inflection, c]),
  ...Object.keys(lodash).filter(i => /case|lower|upper$/gi.test(i)).map(c => [lodash, c])
].reduce((a, [fn, c]) => ({
  ...a,
  [c]: (obj) => {
    if (typeof obj === 'string') return fn[c](obj)

    if (Array.isArray(obj)) return obj.map(o => fn[c](o))

    return lodash.transform(obj, (result, value, key) => {
      if (key && !key.startsWith('"')) result[fn[c](key)] = value
    }, {})
  }
}), {})

module.exports = require('app/core/proxy')([
  global.$.helpers,
  require('./path'),
  require('./util'),
  cases,
  lodash,
  inflection
])
