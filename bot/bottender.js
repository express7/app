'use strict'

const debug = require('debug')('app:bot')
const bot = require('bottender')

module.exports = ({ platform: name } = {}) => {
  // require platform from driver
  const platform = require(`messaging-api-${name}`)
  // get client class
  const Client = platform[Object.keys(platform).find(x => x.endsWith('Client'))]

  return Client
}
