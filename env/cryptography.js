'use strict'

const debug = require('debug')('app:env:crypto')
const crypto = require('crypto')
const fs = require('node:fs')

module.exports.decrypt = function (options) {
  try {
    const secret = options.secret || 'mySecret'
    const inputFile = options.file || '.env.enc'
    const decryptionAlgo = options.decryptionAlgo || 'aes256'
    const ivLength = options.ivLength || 16
    if (!fs.existsSync(inputFile)) throw "".concat(inputFile, " does not exist.")
    if (!secret || typeof secret !== 'string') throw 'No SecretKey provided.'
    const fileBuffer = fs.readFileSync(inputFile)
    const iv = fileBuffer.slice(0, ivLength)
    const ciphertext = fileBuffer.slice(ivLength, fileBuffer.length)
    const key = crypto.createHash('sha256').update(String(secret)).digest()
    const decipher = crypto.createDecipheriv(decryptionAlgo, key, iv)

    let decrypted = decipher.update(ciphertext, 'hex', 'utf8')
    decrypted += decipher["final"]('utf8')

    return decrypted
  } catch (e) {
    debug(e, 'error')
  }
}

module.exports.encrypt = function (options) {
  try {
    const secret = options.secret || 'mySecret'
    const inputFile = options.inputFile || '.env'
    const outputFilePath = options.outputFile || "".concat(inputFile, ".enc")
    const encryptionAlgo = options.encryptionAlgo || 'aes256'
    const ivLength = options.ivLength || 16 // presumably createCipheriv() should work for all the algo in ./openssl_list-cipher-algorithms.csv with the right key/iv length

    if (!fs.existsSync(inputFile)) throw "Error: ".concat(inputFile, " does not exist.")
    if (!secret || typeof secret !== 'string') throw 'No SecretKey provided.Use -s option to specify secret'
    const key = crypto.createHash('sha256').update(String(secret)).digest() // node v10.5.0+ should use crypto.scrypt(secret, salt, keylen[, options], callback)

    const iv = crypto.randomBytes(ivLength)
    const cipher = crypto.createCipheriv(encryptionAlgo, key, iv)
    const output = fs.createWriteStream(outputFilePath)
    output.write(iv)
    fs.createReadStream(inputFile).pipe(cipher).pipe(output)

    output.on('finish', function () {
      console.log('info:', "The Environment file \"".concat(inputFile, "\" has been encrypted to \"").concat(outputFilePath, "\"."))
      console.log('warn:', "Make sure to delete \"".concat(inputFile, "\" for production use."))
    })
  } catch (e) {
    debug(e, 'error')
  }
}