'use strict'

const cryptography = require('./cryptography')
const { parse } = require('dotenv')

module.exports = function (options) {
  try {
    const decryptedContent = cryptography.decrypt({
      secret: options.secret,
      file: options.path,
      decryptionAlgo: options.enc_algo
    })

    if (decryptedContent) {
      const env = {}
      const parsedObj = parse(decryptedContent)

      Object.keys(parsedObj).forEach(function (key) {
        if (!env.hasOwnProperty(key)) {
          env[key] = parsedObj[key]
        }
      })

      return env
    }
  } catch (e) {
    debug(e, 'error')
  }
}
