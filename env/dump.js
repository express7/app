'use strict'

const fs = require('node:fs')
const path = require('node:path')

module.exports = (options = {}) => {
  const envs = { ...options, ...process.env }
  const uniq = [...Object.keys(options)]
  const keys = { 'app': [...uniq] }
 
  let output = ''

  for (const folder of ['./config', './node_modules/app/config']) {
    for (const file of fs.readdirSync(folder)) {
      if (path.extname(file) !== '.js') continue

      const group = path.basename(file, '.js')

      const content = fs.readFileSync(path.join(folder, file), 'utf-8')
      const lines = content.split('\n')
        .filter(c => c.indexOf('Env.get(') > -1)
        .map(c => c.replace(/^.+Env\.get\([\'\"](.+?)[\'\"].*/g, '$1'))

      for (const key of lines) {
        if (!uniq.includes(key)) {
          uniq.push(key)
          keys[group] = keys[group] ? [...keys[group], key] : [key]
        }
      }
    }
  }

  for (const group in keys) {
    output += `## ${group} ##\n`

    for (const key of keys[group].sort()) {
      output += (envs[key] ? `${key}=${envs[key]}`: `# ${key}=`) + '\n'
    }

    output += '\n'
  }

  return output
}
