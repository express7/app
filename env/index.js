'use strict'

const debug = require('debug')('app:env')
const { config, populate } = require('dotenv')

config().parsed

const get = (prop, defaultValue) => process.env[prop] || defaultValue

exports.get = get

if (JSON.parse(get('ENV_ENC', true))) {
  const fs = require('node:fs')
  const path = require('node:path')
  const { machineIdSync } = require('node-machine-id')
  const secure = require('./secure')
  const { encrypt, decrypt } = require('./cryptography')
  const dump = require('./dump')

  const environments = {
    APP_KEY: [...Array(8)].map(x => Math.random().toString(36).slice(2)).join('').slice(0, 64),
    APP_URL: 'http://${host}:${port}',
    HOST: '0.0.0.0',
    PORT: 8080,
    NODE_ENV: 'development'
  }

  debug('constructor')

  // default secret
  const secret = machineIdSync()

  // generate env file if not exist
  const envfile = path.resolve(process.cwd(), '.env')
  if (!fs.existsSync(envfile)) fs.writeFileSync(envfile, dump(environments))

  // generate encrypted env file
  if (!fs.existsSync(envfile + '.enc')) encrypt({ secret })

  try {
    populate(process.env, secure({ secret }))
  } catch (e) {
    // config().parsed
  }

  exports.encrypt = (options = { secret }) => encrypt(options)

  exports.decrypt = (options = { secret }) => decrypt(options)
}
