'use strict'

const debug = require('debug')('app:database:crud')
const _ = require('app/helpers')
const Database = require('.')

debug('constructor')

module.exports = {
  async save (data, options = {}) {
    debug('-> save options:', options, data)

    const {
      connection = this.connection,
      trx = Database.driver(connection),
      knex = trx,
      table = this.table,
      schema = this.schema,
      keys = this.primaryKeys,
      overwrite = true,
      timestamp = true,
      columns = Object.values(this.columns),
      before,
      after
    } = options

    // run before
    if (before) await before

    // timestamp
    const timestamps = !timestamp ? {} : { created_at: knex.fn.now(), updated_at: knex.fn.now() }

    // sanitize data
    const cdata = (Array.isArray(data) ? data : [data])
      .map(r => ({
        ...timestamps,
        ...Object.entries(data).reduce((a, [k, v]) => ({ ...a, [k]: v || undefined }), {})
      }))

    // insert data
    const query = knex(table)
      .insert(cdata, '*')
      .returning('*')

    // add schema
    if (schema) query.withSchema(schema)

    // if conflict
    if (overwrite && keys) {
      // merge columns
      const mergeColumns = !timestamp 
        ? undefined 
        : _.chain([...Object.values(columns), 'updated_at'])
          .difference([...keys, 'created_at', 'createdAt'])
          .uniq()
          .sort()
          .value()

      query.onConflict(keys).merge(mergeColumns)
    }

    // get rows
    const result = await query

    // run after
    if (after) await after

    debug('save ->', result)
    return result
  },

  async remove (filter, options = {}) {
    const {
      connection = this.connection,
      trx = Database.driver(connection),
      knex = trx,
      table = this.table,
      schema = this.schema,
      keys = this.primaryKeys,
      before,
      after
    } = options

    // run before
    if (before) await before

    const query = knex(table)
      .del()
      .where(filter)
      .returning(keys)

    // add schema
    if (schema) query.withSchema(schema)

    // get rows
    const result = await query

    // run after
    if (after) await after

    return result
  }
}
