'use strict'

const debug = require('debug')('app:database:cache')
const _ = require('app/helpers')
const hash = require('object-hash')
const Cache = require('app/cache')

const query = (_query, client, connection, queryParam) => {
  queryParam.__hrtime = process.hrtime()

  return _query.apply(client, [connection, queryParam])
}

module.exports = (driver, options = {}) => {
  const { cacheMinutes, cacheMillis = 0 } = options

  // preprocess query
  const _query = driver.client.query

  driver.client.query = (connection, queryParam) => {
    const { connectionParameters } = connection
    const { __knexQueryUid, ...param } = queryParam

    // added hash key
    queryParam.__hashKey = hash({ 
      connection: _.pickBy(connectionParameters, _.identity),
      query: _.pickBy(param, _.identity)
    })

    return Cache.get(queryParam.__hashKey)
      .then(val => val ? val : query(_query, driver.client, connection, queryParam))
  }

  // process query
  const _processResponse = driver.client.processResponse

  driver.client.processResponse = (obj, runner) => {
    const diff = process.hrtime(obj.__hrtime)
    const ms = diff[0] * 1e3 + diff[1] * 1e-6

    // make cache when takes time
    return (ms < cacheMillis)
      ? _processResponse.apply(driver.client, [obj, runner])
      : Cache.put(obj.__hashKey, { response: obj.response }, cacheMinutes)
        .then(b => _processResponse.apply(driver.client, [obj, runner]))
  }

  return driver
}
