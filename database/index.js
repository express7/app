const debug = require('debug')('app:database')
const _ = require('app/helpers')
const { deepParseJson } = require('deep-parse-json')

// plugin
require('./plugin')
_.require('database/plugins', undefined)

const convert = data => _.camelCase(deepParseJson(_.pickBy(data, _.identity)))

const postProcessResponse = (result, queryContext) => {
  if (typeof result !== 'object') return result

  const { rows = result } = result

  return Array.isArray(rows) ? rows.map(row => convert(row)) : convert(rows)
}

const loader = (name, options) => {
  const config = {
    postProcessResponse,
    compileSqlOnError: false,
    migrations: { directory: './database/migrations' },
    seeds: { directory: './database/seeds' },
    client: name,
    ...options
  }

  // create knex with new driver
  const database = require('knex')(config)

  // limit
  const { limit } = options
  if (limit) database.on('start', builder => {
    if (!builder._single.limit) builder.limit(limit)
  })

  // logger
  if (options.logEnable) require('./log')(database)

  // cache
  if (options.cacheMinutes) require('./cache')(database, config)

  return database
}

module.exports = require('app/core/provider')('database', { loader })
