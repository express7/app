'use strict'

const debug = require('debug')('knex:client:duckdb')
const _ = require('lodash')
const Client = require('knex/lib/dialects/postgres')
const ColumnCompiler = require('./schema/duckdb-columncompiler')

class Client_DuckDB extends Client {
  _driver () {
    return require('duckdb')
  }

  columnCompiler() {
    return new ColumnCompiler(this, ...arguments)
  }

  database() {
    if (!this._database) {
      this._database = new this.driver.Database(this.connectionSettings.filename)
    }
  
    return this._database
  }
  
  acquireRawConnection() {
    return this.database().connect()
  }

  _formatBindings(bindings) {
    if (!bindings) {
      return [];
    }
    return bindings.map((binding) => {
      if (binding instanceof Date) return binding.toJSON()

      if (typeof binding === 'object') return JSON.stringify(binding)

      return binding || undefined
    });
  }

  // Runs the query on the specified connection, providing the bindings and any
  // other necessary prep work.
  async _query(connection, obj) {
    if (!obj.sql) throw new Error('The query is empty');

    const bindings = this._formatBindings(obj.bindings)

    // prepare statement
    const statement = await new Promise((s, j) => connection.prepare(obj.sql, (err, res) => err ? j(err) : s(res)))

    // get response
    const response = await new Promise((s, j) => statement.all(...bindings, (err, res) => err ? j(err) : s(res || true)))

    if (response) obj.response = { rows: response }

    return obj
  }
}
 
Object.assign(Client_DuckDB.prototype, {
  // The "dialect", for reference .
  driverName: 'duckdb'
})

module.exports = Client_DuckDB
