'use strict'

const ColumnCompiler = require('knex/lib/dialects/postgres/schema/pg-columncompiler.js')

class ColumnCompiler_DuckDB extends ColumnCompiler {
  increments(options = { primaryKey: true, isBig: false }) {
    const name = `${this.tableCompiler.tableNameRaw}_${this.getColumnName()}_serial`

    this.tableCompiler.pushQuery(`create sequence ${name}`)

    return (
      `${options.isBig ? 'big' : ''}integer default nextval('${name}')` +
      (this.tableCompiler._canBeAddPrimaryKey(options) ? ' primary key' : '')
    )
  }

  bigincrements(options = { primaryKey: true, isBig: true }) {
    return this.increments(options)
  }

  timestamp() {
    const { args: { 1: { useTz } = {} } = {} } = this
    if (useTz) this.args[1] = { useTz }

    return 'timestamp'
  }
}

module.exports = ColumnCompiler_DuckDB
