'use strict'

const debug = require('debug')('app:database:query')
const _ = require('app/helpers')
const Config = require('app/config')
const Database = require('.')

debug('constructor')

const select = (filter = {}, options = {}) => {
  const {
    connection,
    trx = Database.driver(connection),
    knex = trx,
    table,
    schema,
    column,
    columns = column,
    join,
    joins = join,
    group
  } = options

  let { groups = group } = options

  const query = knex(table)
  
  // check table
  if (typeof table !== 'string') {
    if (table.raw) {
      query.fromRaw(table.raw)
    } else {
      query.fromFn(table)
    }
  }

  // add schema
  if (schema) query.withSchema(schema)

  // groups
  if (groups) {
    if (!Array.isArray(groups)) groups = [groups]
    query.column(groups)
    query.groupBy(groups)
  }

  // joins
  for (const j of Array.isArray(joins) ? joins : [joins]) {
    if (j) {
      if (typeof j === 'string') {
        query.joinRaw(j)
      } else {
        query.join(j.table, j.first, j.second)
      }
    }
  }

  // filter
  if (columns) {
    // filter array
    const filterArray = _.chain(filter)
      .pickBy(f => Array.isArray(f))
      .pick(Object.values(columns || {}))
      .pickBy(_.identity)
      .value()

    for (const key in filterArray) query.whereIn(key, filterArray[key])

    // filter
    const filterSingle = _.chain(filter)
      .omit(Object.keys(filterArray))
      .pick(Object.values(columns || {}))
      .pickBy(_.identity)
      .value()

    if (Object.keys(filterSingle).length > 0) query.where(filterSingle)
  }

  return query.clone()
}

module.exports = {
  async count (filter = {}, options = {}) {
    const {
      connection = this.connection,
      table = this.table,
      schema = this.schema,
      column = this.columns
    } = options

    // get count
    const query = select(filter, { connection, table, schema, column })
      .clear('order')
      .count('*', { as: 'count' })

    const result = await query
    const { rows = result } = result
    const [{ count = 0 } = {}] = rows

    return count
  },

  async find (filter = {}, options = {}) {
    debug('-> find', options.table || this.table, 'options', options, filter)

    const {
      connection = this.connection,
      table = this.table,
      schema = this.schema,
      column = this.columns,
      order,
      orders = order,
      offset = 0,
      limit = Config.get('database.limit', 10),
      before,
      after
    } = options

    // run before
    if (before) await before

    const query = select(filter, { connection, table, schema, column })
      .offset(offset)
      .limit(limit)

    // add orders
    if (orders) query.orderBy(orders)

    // get rows
    const result = await query
    const { rows = result } = result

    // run after
    if (after) await after

    debug('find ->', options.table || this.table, rows)
    return rows
  }
}
