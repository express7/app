'use strict'

const debug = require('debug')('app:database:log')
const logger = require('app/logger')
const Config = require('app/config')

const fields = Config.get('database.log', {
  timeStamp: () => new Date().toJSON(),
  ipAddress: ({ driver }) => driver.client.config.connection.host || '127.0.0.1',
  status: ({ status }) => status,
  userId: ({ driver }) => driver.client.config.connection.user,
  service: () => 'sql',
  protocol: ({ driver }) => driver.client.driverName,
  method: ({ query }) => query.method,
  resource: ({ query }) => query.sql,
  // byteSent: () => ':req[content-length]',
  // byteRecv: () => ':res[content-length]',
  duration: ({ duration }) => `${duration} ms`
})

module.exports = (driver, options) => {
  const uids = {}

  const format = (status, driver, query) => {
    const Model = require('app/model/log')
    const diff = process.hrtime(uids[query.__knexQueryUid])
    const ms = diff[0] * 1e3 + diff[1] * 1e-6

    delete uids[query.__knexQueryUid]

    const { connection: { host, port, database, filename } = {} } = driver.client.config

    const log = Object.keys(Model.props).map(k => fields[k] ? fields[k]({ status, driver, query, duration: ms }) : '-')

    return log.join('\t') + '\n'
  }

  driver.on('query', query => uids[query.__knexQueryUid] = process.hrtime())

  driver.on('query-error', (error, query) => logger.write(format(500, driver, query)))

  driver.on('query-response', (data, query) => logger.write(format(200, driver, query)))

  return driver
}
