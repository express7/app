'use strict'

const debug = require('debug')('app:database:plugin')
const knex = require('knex')
const _ = require('app/helpers')
const Config = require('app/config')

debug('constructor')

const plugins = {
  paginate: function (req, res) {
    const {
      limit = +Config.get('database.limit', 10),
      items = +limit,
      page = 1,
      start = (+page - 1) * +items,
      offset = +start,
      end = +offset + +items,
    } = req ? req.all() : {}

    // count
    const countQuery = this.clone()
      .clear('select')
      .clear('group')
      .clear('order')
      .count('*', { as: 'count' })
      .limit(1)

    // // group or no --> error left join karena ada function berenama type juga, s.type jadi bermasalah
    // if (!!this._statements.find(s => (s.type || '').startsWith('group'))) {
    //   countQuery.fromRaw(`(${this.clone().clear('limit').toString()})`)
    // }

    // add offset limit of original sql
    this.offset(+offset).limit(+items)

    return Promise.all([countQuery, this]).then(([c, q]) => {
      const { rows: [{ count: total }] = c } = c
      const pages = Math.floor(total / +items) + 1

      try {
        // set as headers
        if (res) res.set(Object.entries({ total, pages, items }).reduce((a, [k, v]) => ({ ...a, [`X-${_.capitalize(k)}`]:v }), {}))
      } catch (err) {
        debug(err)
      }

      const { rows = q } = q
      rows.total = +total
      rows.pages = +pages
      rows.items = +items

      return rows
    })
  },

  fromRaw: function (value) {
    return this.from(this.client.raw(`${value}`))
  },

  fromFn: function (fn, params = {}, options = {}) {
    const {
      fn: fnc = fn,
      params: prm = params,
      options: opt = options
    } = fn
        
    const schema = this._single.schema ? `"${this._single.schema}".` : ''

    const filterPrm = (Array.isArray(prm) ? prm.map((c, i) => [i, c]) : Object.entries(prm))
      .filter(p => (typeof p[1] !== 'undefined'))
      .map(p => [p[0], Array.isArray(p[1]) ? `'${p[1].join(',')}'` : (typeof p[1] === 'string' ? `'${p[1]}'` : p[1])])
      
    const prms = !Array.isArray(prm) && !options.argsArray ? filterPrm.map(p => `${p[0]} := ${p[1]}`) : filterPrm.map(p => p[1])

    return this.from(this.client.raw(`${schema}"${fnc}"(${prms.join(', ')})`))
  },

  nest: function (query, joinFrom, joinTo) {
    joinFrom = _.camelCase(joinFrom)
    joinTo = _.camelCase(joinTo)

    query = Object.getPrototypeOf(query).name === 'Model' ? query.query() : query
    query = typeof query === 'string' ? require('database')(query) : query

    const childTableName = query._single.table

    return this.then(async (rows) => {
      const parentIds = rows.map(result => _.camelCase(result)[joinFrom]).filter((v, i, s) => s.indexOf(v) === i)
      const relations = await query.whereIn(joinTo, parentIds)

      return rows.map(row => {
        row[childTableName] = relations.filter(r => r[joinTo] === row[joinFrom])

        return row
      })
    })
  },
  
  tile: function (column = 'geom', x, y, z) {
    const tolerance = +(0.7 / (2 ^ +z)).toFixed(5)

    return this.select(this.client.raw(`st_asgeojson(st_transform(st_snaptogrid(st_simplify(${column}, ${tolerance}), ${tolerance}), 3857)) AS geometry`))
      .whereRaw(`${column} && st_transform(st_tileenvelope(${z}, ${x}, ${y}), 4326)`)
  },

  tileDistinct: function (column = 'geom', x, y, z, { minZoom = 16 }) {
    if (z > +minZoom) return this

    const tolerance = +(0.7 / (2 ^ +z)).toFixed(5)
    
    return this.distinctOn([this.client.raw(`st_snaptogrid(st_simplify(${column}, ${tolerance}), ${tolerance})`)])
  },
  
  grid: function (column = 'grid', x, y, z) {
    return this.select([column])
      .whereRaw(`${column} like rpad(ST_TileQuadkey(${z}, ${x}, ${y}), ${z}, '03') || '%'`)
  }
}

for (const key in plugins) {
  try {
    knex.QueryBuilder.extend(key, plugins[key])
  } catch (e) {}
}
