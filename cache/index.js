const debug = require('debug')('app:cache')

module.exports = require('app/core/provider')('cache')
