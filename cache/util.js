'use strict'

module.exports = {
  serialize (data) {
    return JSON.stringify(data)
  },

  deserialize (data) {
    return JSON.parse(data)
  },

  async valueOf (value) {
    if (typeof value === 'function') {
      value = value()
    }

    return value
  },

  /**
   * Returns integer number between two numbers (inclusive)
   *
   * @param {int} min
   * @param {int} max
   * @return int
  */
  randomIntBetween (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
  }
}
