'use strict'

const debug = require('debug')('app:cache:file-store')
const path = require('node:path')
const fs = require('node:fs')
const ObjectStore = require('./object')

// /**
//  * adonis-cache
//  *
//  * (c) Hany El Nokaly <hany.elnokaly@gmail.com>
//  *
//  * For the full copyright and license information, please view the LICENSE
//  * file that was distributed with this source code.
// */

// const TaggableStore = require('./TaggableStore')
// const Util = require('../Util')

module.exports = class FileStore extends ObjectStore {
  constructor ({ path } = {}) {
    debug('constructor')
    super()
    delete this._storage
    this.path = path

    // ensure log directory exists
    fs.existsSync(this.path) || fs.mkdirSync(this.path, { recursive: true })
  }

  /**
   * Retrieve an item from the cache by key.
   *
   * @param  {string} key
   * @return {Promise<mixed>}
   */
  async get (key) {
    const filePath = path.join(this.path, key)

    if (!fs.existsSync(filePath)) return null

    // if (Date.now() >= fs.statSync(key).mtimeMs) {
    //   await this.forget(key)
    //   return null
    // }

    return fs.readFileSync(filePath)
  }

  /**
   * Store an item in the cache for a given number of minutes.
   *
   * @param  {string}  key
   * @param  {mixed}     value
   * @param  {int|float}     minutes
   * @return {Promise<void>}
   */
  async put (key, value, minutes = this._minutes) {
    if (!value) return 1

    try {
      const filePath = path.join(this.path, key)

      fs.writeFileSync(filePath, value)

      const expiration = new Date()
      expiration.setTime(expiration.getTime() + minutes * 60 * 1000)
      fs.utimesSync(filePath, expiration, expiration)
    } catch (e) {}
  }

  async forget (key) {
    try {
      const filePath = path.join(this.path, key)

      fs.unlinkSync(filePath)
    } catch (e) {}

    return true
  }
}
