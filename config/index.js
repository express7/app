const debug = require('debug')('app:config')
const dotProp = require('dot-prop')
const _ = require('app/helpers')

exports.get = (prop, defaultValue) => {
  const [propm] = prop.split('.')
  debug(prop, '<-', propm)

  const configs = _.mergeWith(
    { [propm]: global.require(`app/config/${propm}`, {}) },
    { [propm]: global.require(`app/${propm}/config`, {}) },
    global.$.config,
    (target, source) => Array.isArray(source) ? source : undefined
  )
    
  const p = prop.split('.').map(p => _.camelCase(p)).join('.')
  const config = dotProp.get(configs, p, defaultValue)

  debug(prop, '->', config)
  return config
}
