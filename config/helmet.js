'use strict'

module.exports = {

  /*
  |--------------------------------------------------------------------------
  | Map Required Models
  |--------------------------------------------------------------------------
  |
  | When utilizing a RAM based store, there might be other applications
  | utilizing the same cache. So, we'll specify a value to get prefixed
  | to all our keys so we can avoid collisions.
  |
  */

  contentSecurityPolicy: false

}
