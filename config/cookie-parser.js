'use strict'

const Env = require('app/env')

module.exports = Env.get('APP_KEY')
