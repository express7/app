'use strict'

const express = require('express')

module.exports = [
  'app/middleware/access-log',
  'app/middleware/morgan-dev',
  express.json,
  express.urlencoded,
  'cookie-parser',
  'helmet',
  'cors',
  'compression'
]
