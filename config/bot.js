'use strict'

const Env = require('app/env')

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Default disk
  |--------------------------------------------------------------------------
  |
  | The default disk is used when you interact with the file system without
  | defining a disk name
  |
  */
  default: Env.get('BOT_PLATFORM', 'bard'),

  /*
  |--------------------------------------------------------------------------
  | Local
  |--------------------------------------------------------------------------
  |
  | Local disk interacts with the a local folder inside your application
  |
  */
  messenger: {
    driver: 'bottender',
    platform: 'messenger',
    pageId: Env.get('MESSENGER_PAGE_ID'),
    accessToken: Env.get('MESSENGER_ACCESS_TOKEN'),
    appId: Env.get('MESSENGER_APP_ID'),
    appSecret: Env.get('MESSENGER_APP_SECRET'),
    verifyToken: Env.get('MESSENGER_VERIFY_TOKEN')
  },

  whatsapp: {
    driver: 'bottender',
    platform: 'whatsapp',
    accountSid: Env.get('WHATSAPP_ACCOUNT_SID'),
    authToken: Env.get('WHATSAPP_AUTH_TOKEN'),
    phoneNumber: Env.get('WHATSAPP_PHONE_NUMBER')
  },

  line: {
    driver: 'bottender',
    platform: 'line',
    accessToken: Env.get('LINE_ACCESS_TOKEN'),
    channelSecret: Env.get('LINE_CHANNEL_SECRET')
  },

  slack: {
    driver: 'bottender',
    platform: 'slack',
    accessToken: Env.get('SLACK_ACCESS_TOKEN'),
    signingSecret: Env.get('SLACK_SIGNING_SECRET')
  },

  telegram: {
    driver: 'bottender',
    platform: 'telegram',
    accessToken: Env.get('TELEGRAM_ACCESS_TOKEN')
  },

  viber: {
    driver: 'bottender',
    platform: 'viber',
    accessToken: Env.get('VIBER_ACCESS_TOKEN')
  },

  bard: {
    driver: 'bard-ai',
    cookie: Env.get('BARD_COOKIE')
  }
}
