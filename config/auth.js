'use strict'

const Env = require('app/env')

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Authenticator
  |--------------------------------------------------------------------------
  |
  | Authentication is a combination of serializer and scheme with extra
  | config to define on how to authenticate a user.
  |
  | Available Schemes - basic, session, jwt, api
  | Available Serializers - lucid, database
  |
  */

  default: 'jwt',
  authenticator: 'jwt',

  /*
  |--------------------------------------------------------------------------
  | Session
  |--------------------------------------------------------------------------
  |
  | Session authenticator makes use of sessions to authenticate a user.
  | Session authentication is always persistent.
  |
  */
  session: {
    scheme: 'session',
    model: 'app/model/user',
    uid: 'email',
    password: 'password'
  },

  /*
  |--------------------------------------------------------------------------
  | Basic Auth
  |--------------------------------------------------------------------------
  |
  | The basic auth authenticator uses basic auth header to authenticate a
  | user.
  |
  | NOTE:
  | This scheme is not persistent and users are supposed to pass
  | login credentials on each request.
  |
  */
  basic: {
    scheme: 'basic',
    model: 'app/model/user',
    uid: 'email',
    password: 'password'
  },

  /*
  |--------------------------------------------------------------------------
  | Jwt
  |--------------------------------------------------------------------------
  |
  | The jwt authenticator works by passing a jwt token on each HTTP request
  | via HTTP `Authorization` header.
  |
  */
  jwt: {
    driver: 'jwt',
    scheme: 'jwt',
    model: 'app/model/user',
    confirmMode: 'email',
    secret: Env.get('APP_KEY'),
    usernameField: 'username',
    emailField: 'email',
    mobileField: 'mobile',
    passwordField: 'password',
    statusField: 'status',
    loginAtField: 'loginAt',
    payloadFields: ['id', 'status', 'roles'],
    checksumFields: ['username', 'loginAt'],
    newState: 'pending',
    activeState: 'active',
    tokenParam: 'token',
    tokenExpiry: '24h',
    confirmExpiry: '1d',
    refreshExpiry: '90d',
    cookieExpiry: 600,
    // options: { jwtid: () => require('app/auth').jwtid }
  }
}
