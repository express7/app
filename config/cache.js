'use strict'

const { join } = require('node:path')
const _ = require('app/helpers')
const Env = require('app/env')

module.exports = {

  /*
  |--------------------------------------------------------------------------
  | Default Cache Store
  |--------------------------------------------------------------------------
  |
  | This option controls the default cache store that gets used while
  | using this caching library. This store is used when another is
  | not explicitly specified when executing a given caching function.
  |
  */

  default: Env.get('CACHE_STORE', 'object'),

  /*
  |--------------------------------------------------------------------------
  | Cache Stores
  |--------------------------------------------------------------------------
  |
  | Here you may define all of the cache "stores" for your application as
  | well as their drivers. You may even define multiple stores for the
  | same cache driver to group types of items stored in your caches.
  |
  | Supported drivers: "object", "database", "redis"
  | Hint: Use "null" driver for disabling caching
  |
  */

  object: {
    driver: 'object'
  },

  null: {
    driver: 'null'
  },

  file: {
    driver: 'file',
    path: join(_.tmpPath(), 'cache')
  },

  // database: {
  //   driver: 'database',
  //   table: 'cache',
  //   connection: 'sqlite'
  // },

  redis: {
    driver: 'redis',
    connection: {
      host: Env.get('REDIS_HOST', '127.0.0.1'),
      port: Env.get('REDIS_PORT', 6379),
      password: Env.get('REDIS_PASSWORD', null),
      db: 0,
      keyPrefix: ''
    }
  },

  /*
  |--------------------------------------------------------------------------
  | Cache Key Prefix
  |--------------------------------------------------------------------------
  |
  | When utilizing a RAM based store, there might be other applications
  | utilizing the same cache. So, we'll specify a value to get prefixed
  | to all our keys so we can avoid collisions.
  |
  */

  prefix: 'cache',

  /*
  |--------------------------------------------------------------------------
  | Cache Expiry
  |--------------------------------------------------------------------------
  |
  | When utilizing a RAM based store, there might be other applications
  | utilizing the same cache. So, we'll specify a value to get prefixed
  | to all our keys so we can avoid collisions.
  |
  */

  minutes: 10
}
