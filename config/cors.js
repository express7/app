'use strict'

module.exports = {
  // exposed headers
  exposedHeaders: ['date', 'x-total', 'x-pages', 'x-items'],

  // credentials
  credentials: true
}
