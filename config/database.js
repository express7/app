'use strict'

const Env = require('app/env')

module.exports = {
  // Connection defines the default connection settings to be used while interacting with SQL databases.
  default: Env.get('DB_CONNECTION', 'duckdb'),

  queryTimeout: 30000,

  // Limit rows
  // limit: 10,

  // Sqlite is a flat file database and can be good choice under development environment.
  sqlite: {
    driver: 'sqlite3',
    connection: {
      // filename: Helpers.databasePath(`${Env.get('DB_DATABASE', 'development')}.sqlite`)
    },
    useNullAsDefault: true
  },
    
  // Here we define connection settings for DuckDB database.
  duckdb: {
    driver: 'duckdb',
    client: require('app/database/duckdb'),
    connection: {
      filename: ':memory:'
    }
  },

  // Here we define connection settings for MySQL database.
  mysql: {
    driver: 'mysql',
    connection: {
      host: Env.get('DB_HOST', 'localhost'),
      port: Env.get('DB_PORT', ''),
      user: Env.get('DB_USER', 'root'),
      password: Env.get('DB_PASSWORD', ''),
      database: Env.get('DB_DATABASE', 'database')
    }
  },

  // Here we define connection settings for PostgreSQL database.
  pg: {
    driver: 'pg',
    connection: {
      host: Env.get('DB_HOST', 'localhost'),
      port: Env.get('DB_PORT', ''),
      user: Env.get('DB_USER', 'root'),
      password: Env.get('DB_PASSWORD', ''),
      database: Env.get('DB_DATABASE', 'database'),
      application_name: 'app',
      connectionTimeoutMillis: 10 * 60 * 1000,
      idleTimeoutMillis: 10 * 60 * 1000,
      statement_timeout: 9 * 60 * 1000
    },
    
    // searchPath: [Env.get('PG_SCHEMA', 'public'), 'public'],

    // Connection pools configuration
    // pool: {
    //   min: 1,
    //   max: 10
    // },

    acquireConnectionTimeout: 10000,

    // cacheMinutes Minutes before cache are expired.
    // cacheMinutes: 2,

    // cacheMillis Milliseconds time length data retrieve threshold to be cached.
    // cacheMillis: 5000,
  }
}
