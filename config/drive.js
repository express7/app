'use strict'

const path = require('path')
const _ = require('app/helpers')
const Env = require('app/env')

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Default disk
  |--------------------------------------------------------------------------
  |
  | The default disk is used when you interact with the file system without
  | defining a disk name
  |
  */
  default: 'local',

  /*
  |--------------------------------------------------------------------------
  | Local
  |--------------------------------------------------------------------------
  |
  | Local disk interacts with the a local folder inside your application
  |
  */
  local: {
    driver: 'app/drive/local',
    root: _.tmpPath()
  },

  /*
  |--------------------------------------------------------------------------
  | Google Drive
  |--------------------------------------------------------------------------
  |
  | Google drive interacts with files on google shared drive using service account
  |
  */

  gdrive: {
    driver: 'gdrive',
    keyFile: path.join(_.rootPath(), 'google_credentials.json'),
    folderId: Env.get('GDRIVE_FOLDER')
  },

  /*
  |--------------------------------------------------------------------------
  | S3
  |--------------------------------------------------------------------------
  |
  | S3 disk interacts with a bucket on aws s3
  |
  */
  s3: {
    driver: 's3',
    key: Env.get('S3_KEY'),
    secret: Env.get('S3_SECRET'),
    bucket: Env.get('S3_BUCKET'),
    region: Env.get('S3_REGION')
  },

  /*
  |--------------------------------------------------------------------------
  | GCS
  |--------------------------------------------------------------------------
  |
  | GCS disk interacts with a bucket on google cloud storage
  |
  */
  gcs: {
    driver: 'gcs',
    keyFilename: Env.get('GCS_KEY_FILE_NAME'), // path to json file
    bucket: Env.get('GCS_BUCKET')
  }
}
