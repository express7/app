'use strict'

const Env = require('app/env')
const _ = require('app/helpers')

module.exports = {
  default: Env.get('QUEUE_CONNECTION', 'app'),

  app: {
    driver: _.app
  },

  // npm i amqplib amqp-connection-manager
  rabbitmq: {
    client: 'rabbitmq',
    driver: 'amqplib',
    queue: 'test-queue',
    connection: {
      // url: 'amqp://ocpusr:immspocp2023@localhost:5672',
      protocol: 'amqp',
      hostname: Env.get('RMQ_HOST', 'localhost'),
      port: Env.get('RMQ_PORT', '5672'),
      username: Env.get('RMQ_USER'),
      password: Env.get('RMQ_PASS'),
      // locale: 'en_US',
      // frameMax: 0,
      // heartbeat: 0,
      // vhost: '/'
    }
  },

  redis: {
    driver: 'redis',
    connection: {
      host: Env.get('REDIS_HOST', '127.0.0.1'),
      port: Env.get('REDIS_PORT', 6379),
      password: Env.get('REDIS_PASSWORD', null),
      db: 0,
      keyPrefix: ''
    }
  }
}
