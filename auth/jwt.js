const debug = require('debug')('app:auth:jwt')
const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const _ = require('app/helpers')
const password = require('./password')

module.exports = options => {
  const { secret, secretOrKey = secret, model, payloadFields, checksumFields } = options

  const User = model.table ? model : require(model)

  // filtered fields
  const columns =  Object.entries(_.omit(User.columns, User.hidden)).reduce((a, [k, v]) => ({ ...a, [k]: v, [v]: v }), {})

  // set configuration
  Object.assign(options, {
    secretOrKey,
    model: User,
    payloadFields: Object.values(_.pick(columns, payloadFields)),
    checksumFields: Object.values(_.pick(columns, checksumFields))  
  })

  // convert any function of jwt options
  const jwtOptions = async (data, opts = {}) => {
    for (const [key, val] of Object.entries({ ...options.options, ...data })) {
      if (typeof val === 'function' && !val.prototype) data[key] = await val()(opts)
    }

    return data
  }

  const token = (req, opts = {}) => req.all()[options.tokenParam]
    || req.signedCookies[options.tokenParam]
    || req.cookies[options.tokenParam]
    || (req.get('authorization') || '').split(' ').pop()
    || req.get(options.tokenParam)
    || req.get('x-' + options.tokenParam)

  const checksum = (user, fields = options.checksumFields) => {
    const payload = _.pickBy(fields.reduce((a, c) => ({ ...a, [c]: user[c] }), {}), _.identity)

    return crypto.createHash('md5')
      .update(JSON.stringify(payload))
      .digest('hex')
  }

  return Object.assign(token, {
    checksum,
    compare: password.compare,
    config: options,
    decode: jwt.decode,
    extract: token,
    hash: password.hash,
    jwtid: async ({ token, id = jwt.decode(token).id }, options = {}) => {
      const [user] = await User.find({ id }, { limit: 1 })
      return checksum(user.toJSON())
    },
    password,
    payload: (payload, fields = [], opts = {}) => _.pickBy(_.pick(payload, [...fields, ...options.payloadFields]), _.identity),
    sign: async (payload, secret, opts = {}) => jwt.sign(payload, secret, await jwtOptions(opts, payload)),
    verify: async (token, secret, opts = {}) => jwt.verify(token, secret, await jwtOptions(opts, { token }))
  })
}
