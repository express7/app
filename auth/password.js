'use strict'

const debug = require('debug')('app:auth:password')
const { hashSync, compareSync: compare} = require('bcryptjs')
const Config = require('app/config')
const rounds = Config.get('auth.rounds', 10)

module.exports = {
  compare,
  hash: password => hashSync(password, rounds)
}
