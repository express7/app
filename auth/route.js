'use strict'

const debug = require('debug')('app:route:auth')
const _ = require('app/helpers')
const ms = require('ms')
const jwt = require('.')
const { Router, auth, check, validate, version } = require('app/router')
const createError = require('http-errors')
const v = version({ default: '1.0.0' })

const {
  model: User,
  secretOrKey,
  confirmMode,
  usernameField,
  emailField,
  mobileField,
  passwordField,
  statusField,
  loginAtField,
  activeState,
  newState,
  tokenParam,
  tokenExpiry: expiresIn,
  cookieExpiry,
  refreshExpiry,
  confirmExpiry,
  options = {}
} = jwt.config

const getUser = async (filter, options = {}) => {
  // remove null filter properties
  filter = _.pickBy(_.pick(filter, Object.keys(User.props)), _.identity)

  // get user
  const [user] = await User.find(filter, { ...options, limit: 1 })

  return user
}

const cookieOptions = {
  maxAge: typeof cookieExpiry === 'string' ? ms(cookieExpiry) : cookieExpiry,
  path: '/',
  httpOnly: true, // cannot be modified using XSS or JS
  secure: true,
  signed: true
}

module.exports = Router({ mergeParams: true })

  /**
   * @api [get] /api/v1/user/register{format}
   * description: "User login"
   * tags:
   *   - user
   * parameters:
   *   - (path) format=.json {String} Output format
   *   - (query) username* {String} Username
   *   - (query) email* {String} Email
   *   - (query) password* {String} Password
   */
   
  // signup
  .all('(/signup|/register).:format?', v('1.0.0'),
    // validation
    check('username').not().isEmpty(),
    check('email').not().isEmpty().isEmail(),
    check('password', '...').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/, 'i'),
    // validate,

    // control
    async (req, res, next) => {
      try {
        if (!['GET', 'POST'].includes(req.method)) return next(createError(404))

        const { email, username = email.split('@')[0], mobile, password } = req.all()

        const existingUser = await getUser({ username, email })

        if (existingUser
          && existingUser[statusField] === activeState
          // && next
        ) return next(createError(409, 'User exists')) 

        // create user
        const user = new User({
          [usernameField]: username,
          [emailField]: email,
          [mobileField]: mobile,
          [passwordField]: jwt.hash(password),
          [statusField]: confirmMode ? newState : activeState
        })

        // save user
        await user.save()

        const payload = jwt.payload(user.toJSON())

        // confirmation token
        if (confirmMode) {
          const token = await jwt.sign(payload, secretOrKey, { ...options, expiresIn: confirmExpiry })

          debug('signup -> confirmation successful', { ...payload, token })
          return res.json({ ...payload, token })
        }

        req.app.emit('auth:signup', payload)

        debug('signup -> successful', payload)
        return res.json(payload)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  // signup confirmation
  .all('/confirm.:format?', v('1.0.0'),
    // control
    async (req, res, next) => {
      try {
        if (!['GET', 'POST'].includes(req.method)) return next(createError(400, 'Resource not available'))
 
        const token = jwt.extract(req)

        const decoded = await jwt.verify(token, secretOrKey, options)
        const { payload: { sub, id = sub } = decoded } = decoded
    
        debug(req.timestamp, '-> confirm', req.user)

        const user = await getUser({ id })

        if (!user) return next(createError(401, 'User not found'))

        const { username } = user

        if (user[statusField] === activeState) return res.json({ id, username, [statusField]: activeState })

        if (user[statusField] === newState) {
          // change state
          user[statusField] = activeState

          // save user
          await user.save()

          req.app.emit('auth:confirm', { id, username, [statusField]: activeState })

          debug(req.timestamp, 'confirm -> successful', { id, username })
          return res.json({ id, username, [statusField]: activeState })
        }

        return next(createError(401, 'Invalid confirmation token'))
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )
  
  /**
   * @api [get] /api/v1/user/login{format}
   * description: "User login"
   * tags:
   *   - user
   * parameters:
   *   - (path) format=.json {String} Output format
   *   - (query) username* {String} Username
   *   - (query) email* {String} Email
   *   - (query) password* {String} Password
   */
  .all('(/signin|/login).:format?', v('1.0.0'),
    // validation
    // check('username').not().isEmpty(),
    check('password', '...').not().isEmpty(),
    validate,

    // control
    async (req, res, next) => {
      try {
        if (!['GET', 'POST'].includes(req.method)) return next(createError(404))
      
        const { email, username = email, password } = req.all()
        debug(req.timestamp, '-> signin', username, password)

        // get data
        const loginField = username.match(/^\d{6,15}$/)
          ? mobileField
          : username.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)
            ? emailField
            : usernameField

        const user = await getUser({ [loginField]: username })

        if (!user) return next(createError(401, 'User not found'))

        // check password
        if (!jwt.compare(password, user[passwordField])) return next(createError(401, 'Wrong Username or Password'))

        // check active
        if (user[statusField] !== activeState) return next(createError(401, 'Profile is still not Active'))

        // update last login
        user[loginAtField] = new Date()

        // save user
        await user.save()

        // hardening payload
        const payload = jwt.payload(user.toJSON())

        // create jwt token
        const token = await jwt.sign(payload, secretOrKey, { ...options, expiresIn })

        const refreshToken = await jwt.sign(payload, secretOrKey, { ...options, expiresIn: refreshExpiry })

        // Set jwt token in cookie as 'access_token'
        res.cookie(tokenParam, token, cookieOptions)

        req.app.emit('auth:signin', { ...payload, token, refreshToken })

        debug(req.timestamp, 'signin -> successful', { token, refreshToken })
        return res.json({ ...payload, token, refreshToken })
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  .all('/refresh.:format?', v('1.0.0'),
    // authorization
    auth(),

    // control
    async (req, res, next) => {
      try {
        if (!['GET', 'POST'].includes(req.method)) return next(createError(404))
 
        debug(req.timestamp, '-> refresh', req.user)

        const user = await getUser({ id: req.user.id })

        // update last login
        user[loginAtField] = new Date()

        // save user
        await user.save()

        // hardening payload
        const payload = jwt.payload(user.toJSON())

        // create jwt token
        const token = await jwt.sign(payload, secretOrKey, { ...options, expiresIn })

        // add refresh token when refresh token is almost expired
        const { user: { exp = 0 } = {} } = req
        if ((Date.now() / 1000 - exp) < 86400) {
          const refreshToken = sign(payload, secretOrKey, { ...options, expiresIn: refreshExpiry })

          req.app.emit('auth:refresh', { ...payload, token, refreshToken })

          debug('refresh -> successful + refresh', { token, refreshToken })
          return res.json({ token, refreshToken })
        }
        
        // Set jwt token in cookie as 'access_token'
        res.cookie(tokenParam, token, cookieOptions)

        req.app.emit('auth:refresh', { ...payload, token })

        debug('refresh -> successful', { token })
        return res.json({ token })
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  .all('(/signout|/logout).:format?', v('1.0.0'),
    // authorization
    auth(),

    // control
    async (req, res, next) => {
      try {
        if (!['GET', 'POST'].includes(req.method)) return next(createError(404))

        debug(req.timestamp, '-> signout', req.user)

        const user = await getUser({ id: req.user.id })

        // update last login
        user[loginAtField] = new Date()

        // save user
        await user.save()

        // Delete jwt token in cookie as 'access_token'
        res.clearCookie(tokenParam)

        const message = 'Sign out successful'

        req.app.emit('auth:signout', { ...req.user })

        debug('signout -> successful', { message })
        return res.json({ message })
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  .all('/me.:format?', v('1.0.0'),
    // authorization
    auth(),

    // control
    async (req, res, next) => res.json({ data: req.user })
  )

  // forgot user password
  .all('/forgot.:format?', v('1.0.0'),
    // validation
    check('email').not().isEmpty().normalizeEmail().isEmail(),
    validate,

    // control
    async (req, res, next) => {
      try {
        if (!['GET', 'POST'].includes(req.method)) return next(createError(404))

        const { email } = req.all()
      
        debug(req.timestamp, '-> forgot', email)
        
        const user = await getUser({ email })

        if (!user && next) return next(createError(400, 'User not found')) 

        const payload = { id: user.id, email}

        // confirmation token
        const token = await jwt.sign(payload, secretOrKey, { ...options, expiresIn: confirmExpiry })

        req.app.emit('auth:forgot', { ...payload, token })
        
        debug('forgot ->', { email, token })
        return res.json({ message: `Email sent to ${email}` })
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )

  // modify
  .all('/modify.:format?', v('1.0.0'),  
    // validation
    // check('username').not().isEmpty(),
    check('password', '...').not().isEmpty(),
    validate,

    // authorization
    auth(),

    // control
    async (req, res, next) => {
      try {
        if (!['GET', 'POST'].includes(req.method)) return next(createError(404))

        debug(req.timestamp, '-> modify', req.user)

        if (!req.user.email) return next(createError(401, 'Invalid jwt'))
        
        const user = await getUser({ email: req.user.email })

        if (!user) return next(createError(401, 'User not found'))

        const { email, password } = req.all()
        
        if (email) user.email = email
        user.password = jwt.hash(password)
        
        // update last login
        user[loginAtField] = new Date()

        // save user
        await user.save()

        const payload = jwt.payload(user.toJSON())

        req.app.emit('auth:modify', payload)

        debug('modify -> successful', payload)
        return res.json(payload)
      } catch (err) {
        return next(createError(500, err))
      }
    }
  )
