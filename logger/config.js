'use strict'

const path = require('path')
const _ = require('app/helpers')

module.exports = {
  default: 'rotator',

  rotator: {
    driver: 'rotator',

    // filename: Filename including full path used by the stream
    filename: path.join(_.tmpPath(), 'log', 'app-%DATE%.log'),

    // frequency: How often to rotate. Options are 'daily' for daily rotation, 'date' based on date_format, '[1-12]h' to rotate every 1-12 hours, '[1-30]m' to rotate every 1-30 minutes.
    frequency: 'daily',

    // verbose: If set, it will use console.log to provide extra information when events happen. Default is false.
    verbose: false,

    // date_format: Use 'Y' for full year, 'M' for month, 'D' for day, 'H' for hour, 'm' for minutes, 's' for seconds If using 'date' frequency, it is used to trigger file change when the string representation changes. It will be used to replace %DATE% in the filename. All replacements are numeric only.
    date_format: 'YYYYMMDD',

    // size: Max size of the file after which it will rotate. It can be combined with frequency or date format. The size units are 'k', 'm' and 'g'. Units need to directly follow a number e.g. 1g, 100m, 20k.
    // size: "100M",

    // max_logs Max number of logs to keep. If not set, it won't remove past logs. It uses its own log audit file to keep track of the log files in a json format. It won't delete any file not contained in it. It can be a number of files or number of days. If using days, add 'd' as the suffix. e.g., '10d' for 10 days.
    // maxLogs: "10",

    // audit_file Location to store the log audit file. If not set, it will be stored in the root of the application.
    audit_file: "/tmp/log/audit.json",

    // end_stream End stream (true) instead of the default behaviour of destroy (false). Set value to true if when writing to the stream in a loop, if the application terminates or log rotates, data pending to be flushed might be lost.
    // endStream: false

    // file_options An object passed to the stream. This can be used to specify flags, encoding, and mode. See https://nodejs.org/api/fs.html#fs_fs_createwritestream_path_options. Default { flags: 'a' }.
    // fileOptions: { flags: 'a' }

    // utc Use UTC time for date in filename. Defaults to 'false'
    // utc: false

    // extension File extension to be appended to the filename. This is useful when using size restrictions as the rotation adds a count (1,2,3,4,...) at the end of the filename when the required size is met.
    // extension: ".log",

    // create_symlink Create a tailable symlink to the current active log file. Defaults to 'FALSE'
    // createSymlink: true,

    // symlink_name Name to use when creating the symbolic link. Defaults to 'current.log'
    // symlinkName: "tail-current.log",

    // audit_hash_type Use specified hashing algorithm for audit. Defaults to 'md5'. Use 'sha256' for FIPS compliance.
    // auditHashType: 'md5'
  }
}
