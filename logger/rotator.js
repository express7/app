'use strict'

const debug = require('debug')('app:logger:rotator')
const fs = require('node:fs')
const path = require('node:path')
const _ = require('app/helpers')
const FileStreamRotator = require('file-stream-rotator')

// create a rotating write stream
const config = {
  date_format: 'YYYYMMDD',
  filename: path.join(_.tmpPath(), 'log', 'app-%DATE%.log'),
  frequency: 'daily',
  verbose: false
}

module.exports = (options = config) => {
  // ensure log directory exists
  const logDirectory = path.dirname(options.filename)
  fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory, { recursive: true })

  return FileStreamRotator.getStream(options) || process.stdout
}
