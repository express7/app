# app

[![npm version](https://img.shields.io/npm/v/express-validator.svg)](https://git.immobisp.com/express/app)
[![Build Status](https://img.shields.io/travis/express-validator/express-validator.svg)](https://git.immobisp.com/express/app)
[![Coverage Status](https://img.shields.io/coveralls/express-validator/express-validator.svg)](https://git.immobisp.com/express/app/-/tree/main)

An [express.js](https://github.com/visionmedia/express) mandatory core
[application](https://git.immobisp.com/express/app).

- [Installation](#installation)
- [Documentation](#documentation)
- [Changelog](#changelog)
- [License](#license)

## Installation

```console
npm install git+https://git.immobisp.com/express/app.git
```

Follow up by registering the middleware inside the config/middleware.js array:

```js
module.exports = [
  'app/middleware/access-log',
  'app/middleware/morgan-dev',
  'app/middleware/toobusy',
  express.json,
  express.urlencoded,
  'cookie-parser',
  'helmet',
  'cors',
  'hpp',
  'compression',
  'oas'
]
```

### Auth Middleware

setup config/auth.js :

```js
module.exports = {
  jwt: {
    scheme: 'jwt',
    model: 'app/models/user',
    confirmMode: 'email',
    secret: Env.get('APP_KEY'),
    usernameField: 'username',
    emailField: 'email',
    mobileField: 'mobile',
    passwordField: 'password',
    statusField: 'status',
    loginAtField: 'loginAt',
    payloadFields: ['id', 'status', 'roles'],
    checksumFields: ['username', 'loginAt'],
    newState: 'pending',
    activeState: 'active',
    tokenParam: 'token',
    tokenExpiry: '24h',
    confirmExpiry: '1d',
    refreshExpiry: '90d',
    options: { jwtid: () => require('app/auth/token').jwtid }
  }
}
```

### CORS Middleware

setup config/cors.js :

```js
module.exports = {
  origin: '*',  
  exposedHeaders: ['date', 'x-page', 'x-total-count'],
  credentials: true
}
```

Also make sure that you have Node.js 8 or newer in order to use it.

## Documentation

Please refer to the documentation website on https://git.immobisp.com/express/app/-/blob/master/README.md.

### Bundled middlewares

- [ ] auth
- [ ] compression
- [ ] cookie-parser
- [ ] cors
- [ ] hpp
- [ ] cache
- [ ] morgan
- [ ] multer
- [ ] restful
- [ ] role
- [ ] toobusy
- [ ] validate
- [ ] version

## Changelog

Check the [GitLab Releases page](https://git.immobisp.com/express/app/-/blob/master/CHANGELOG.md).

## License

MIT License
