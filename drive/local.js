'use strict'

const debug = require('debug')('app:drive:local')
const _ = require('app/helpers')
const fs = require('node:fs')
const { join, dirname, sep } = require('node:path') // require('path/posix')

module.exports = ({ root = _.tmpPath() }) => {
  const apath = path => path.startsWith('/') ? path : join(root, path)

  function find (path = '', options = {}, paths = []) {
    const fullPath = apath(path)
    if (!options.rootPath) options.rootPath = fullPath

    const relPath = fullPath.replace(options.rootPath, '').replace(/^\W+/g, '')

    for (const file of fs.readdirSync(fullPath, options)) {
      const filepath = join(fullPath, file)

      if (fs.statSync(filepath).isDirectory()) {
        paths.push(join(relPath, file, sep))
        find(filepath, options, paths)
      } else {
        paths.push(join(relPath, file))
      }
    }

    return paths
  }

  function list (path, options = {}) {
    const files = typeof path === 'string' ? find(path, options) : path

    return files.map(name => ({
      id: join(options.rootPath, name),
      name,
      parents: join(options.rootPath, name).split(sep).slice(1, name.endsWith(sep) ? -2 : -1)
    }))
  }

  function createFolder (path, options = {}) {
    try {
      fs.mkdirSync(apath(path), { recursive: true })
    
      return path
    } catch (err) {}
  }

  function createFile (path, data, options = {}) {
    createFolder(dirname(path), options)

    const { buffer = data } = data

    return options.stream ? fs.createWriteStream(apath(path), options) : fs.writeFileSync(apath(path), buffer, options)
  }

  function exists (path, options = {}) {
    return fs.existsSync(apath(path))
  } 

  function del (path, options = {}) {
    return fs.unlinkSync(apath(path))
  }

  return ({
    all: list,
    createFolder,
    createFile,
    del,
    exists,
    find,
    files: (path, options) => list(find(path, options).filter(f => !f.endsWith(sep)), options),
    folders: (path, options) => list(find(path, options).filter(f => f.endsWith(sep)), options),
    get: (path, options) => options.stream ? fs.createReadStream(path, options) : fs.readFileSync(path, options),
    list,
    makeFile: createFile,
    makeFolder: createFolder,
    put: createFile,
    copy: (src, dest, options) => fs.copyFileSync(src, dest, options),
    move: (src, dest, options) => {
      fs.copyFileSync(src, dest, options)
      return fs.unlinkSync(src)
    },

    ...fs
  })
}
