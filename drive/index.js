const debug = require('debug')('app:drive')

const loader = (name, options) => {
  if (typeof name !== 'string') return name

  try {
    return require(name)(options)
  } catch (e) {
    const message = `App: run\n$ npm install git+https://git.immobisp.com/express/${name}.git`
    throw new Error(`${message}\n${e.message}`)
  }
}

module.exports = require('app/core/provider')('drive', { loader })
