'use strict'

const debug = require('debug')('app:event')

module.exports = class ClientApp {
  constructor (app) {
    this.channel = app
    for (const e of ['on', 'once', 'emit', 'fire']) {
      if (app[e]) this[e] = app[e]
    }
  }

  async send (notify, payload) {
    this.channel.emit(notify, payload)
    return this
  }

  async recv (notify, payload) {
    this.channel.on(notify, payload)
    return this
  }
}
