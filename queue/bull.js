'use strict'

const debug = require('debug')('app:event:bull')
const Client = require('./client')
const Queue = require('bull')

// create default db
module.exports = class ClientBull extends Client {
  constructor ({ connection, queue }) {
    const { url, ...config } = connection  
    this.config = url ? url : config

    this.queues = {}
  }

  async assertQueue (notify) {
    try {
      let queue = this.queues[notify]

      if (queue) return queue

      queue = new Queue(notify, this.config)

      this.queues[notify] = queue

      return queue
    } catch (err) {
      debug(err)
    }
  }

  async send (notify, payload) {
    try {
      // await this.connect()
      const queue = this.assertQueue(notify)

      queue.add(Buffer.from(JSON.stringify(payload)))

      return this
    } catch (err) {
      debug(err)
    }
  }

  async recv (notify, payload, { ack = true } = {}) {
    try {
      // await this.connect()
      const queue = this.assertQueue(notify)

      queue.process(payload)

      payload(job)

      return this
    } catch (err) {
      debug(err)
    }
  }
}
