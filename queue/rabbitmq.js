'use strict'

const debug = require('debug')('app:event:rabbitmq')
const amqp = require('amqplib') // require('amqp-connection-manager')

// create default db
module.exports = class ClientRabbitMQ {
  constructor ({ connection, queue }) {
    const { url, ...config } = connection  
    this.config = url ? url : config

    this.queues = []
  }

  async connect1 () {
    try {
      this.connection = this.connection || await amqp.connect(this.config)
      this.channel = this.channel || await this.connection.createChannel()

      return this
    } catch (err) {
      debug(err)
    }
  }

  async assertQueue (notify) {
    try {
      if (!this.queues.includes(notify)) {
        await this.channel.assertQueue(notify)
        this.queues.push(notify)
      }

      return this
    } catch (err) {
      debug(err)
    }
  }

  async send1 (notify, payload) {
    try {
      await this.connect()
      await this.assertQueue(notify)
      await this.channel.sendToQueue(notify, Buffer.from(JSON.stringify(payload)))

      return this
    } catch (err) {
      debug(err)
    }
  }

  async recv1 (notify, payload, { ack = true } = {}) {
    try {
      await this.connect()
      await this.assertQueue(notify)

      await this.channel.consume(notify, msg => {
        if (ack) this.channel.ack(msg)
        payload(JSON.parse(msg.content.toString()), msg)
      })

      return this
    } catch (err) {
      debug(err)
    }
  }


  async connect () {
    try {
      this.connection = this.connection || await amqp.connect(this.config)

      return this
    } catch (err) {
      console.log(err)
    }
  }

  async send (notify, payload, options = {}) {
    try {
      await this.connect()
      this.channel1 = this.channel1 || await this.connection.createChannel()
      await this.channel1.assertQueue(notify)

      await this.channel1.sendToQueue(notify, Buffer.from(JSON.stringify(payload)), options)

      return this
    } catch (err) {
      debug(err)
    }
  }

  async recv (notify, payload, { ack = true } = {}) {
    try {
      await this.connect()
      this.channel2 = this.channel2 || await this.connection.createChannel()
      await this.channel2.assertQueue(notify)

      await this.channel2.consume(notify, msg => {
        if (ack) this.channel2.ack(msg)
        payload(JSON.parse(msg.content.toString()), msg)
      })

      return this
    } catch (err) {
      console.log(err)
    }
  }
}
