'use strict'

const debug = require('debug')('app:event:redis')
const Redis = require('ioredis')

// create default db
module.exports = class ClientRedis {
  constructor ({ connection, queue }) {
    const { url, ...config } = connection  
    this.config = url ? url : config

    this.queue = queue
  }

  async connect () {
    return this
  }

  async send (notify = this.queue, payload) {
    try {
      this.connectionp = this.connectionp || new Redis(this.config)

      await this.connectionp.publish('channel', JSON.stringify(payload))

      return this
    } catch (err) {
      debug(err)
    }
  }

  async recv (notify = this.queue, payload, { ack = true } = {}) {
    try {
      this.connection = this.connection || new Redis(this.config)

      if (!this.channel) {
        this.channel = 'channel'
        await this.connection.subscribe(this.channel)
      }

      await this.connection.on('message', (channel, msg) => payload(JSON.parse(msg)))

      return this
    } catch (err) {
      debug(err)
    }
  }
}
