'use strict'

const debug = require('debug')('app:router:param')
const toobusy = require('node-toobusy')

module.exports = options => async (req, res, next) => toobusy(options)
  ? res.status(503).send("I'm busy right now, sorry.")
  : next()
