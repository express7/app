const _ = require('app/helpers')
const { dependencies = {} } = require('root/package.json')
const exclude = /index\.js$/

module.exports = require('app/core/proxy')([
  () => global.$?.middlewares,
  {
    ...require('express-validator'),

    // plugin middlewares
    ...Object.entries(dependencies)
        .filter(([k, v]) => v.endsWith('.git'))
        .map(([k, v]) => _.camelCase(requireDir(`${k}/middleware`, { exclude })))
        .reduce((a, c) => ({ ...a, ...c }), {}),

    // app middleware
    ..._.camelCase(requireDir('app/middleware', { exclude }))
  }
])
