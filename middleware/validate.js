'use strict'

const debug = require('debug')('app:router:validate')
const createError = require('http-errors')
const { validationResult } = require('express-validator')

module.exports = async (req, res, next) => {
  const errors = validationResult(req)

  if (!errors.isEmpty()) return next(createError(400, errors.array()))

  if (next) return next()
}
