'use strict'

const debug = require('debug')('app:cache:middleware')
const path = require('path')
const FileType = require('file-type')
const hash = require('object-hash')
const _ = require('../helpers')
const Cache = require('../cache')

module.exports = (name, { minutes = Cache._minutes } = {}) => {
  debug('constructor %O', name)

  return async (req, res, next) => {
    // cache key
    const props = _.pick(req, ['originalUrl', 'query', 'body', 'user'])
    if (props.user) props.user = props.user.id
    const key = hash(props)

    // res.end if buffer exist
    const cache = await Cache.store(name).get(key)
    if (!!cache) {
      const { mime } = await FileType.fromBuffer(Buffer.from(cache)) || { mime: 'json' }

      debug('%s api -> BUFFER %s %O', req.timestamp, req.url, cache)
      res.type(mime)
      return res.end(Buffer.from(cache, 'base64'))
    }

    // do not cache if status is not 200
    if (res.statusCode !== 200) return next()

    // intercept response end
    const resEnd = res.end

    res.end = function (content, encoding) {    
      if (res.statusCode !== 200) return resEnd.apply(this, arguments)

      // put cache
      debug('%s %s -> put %s minute(s) %O', req.timestamp, key, minutes, content)
      Cache.store(name).put(key, content, minutes)
      debug('%s %s put -> SUCCESSFUL %s minute(s) %O', req.timestamp, key, minutes, content)

      // call original res.end
      debug('%s api -> ORIGIN %s %O', req.timestamp, req.url, content)
      return resEnd.apply(this, arguments)
    }

    return next()
  }
}
