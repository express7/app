'use strict'

const debug = require('debug')('app:middleware:morgan-dev')
const morgan = require('morgan')

module.exports = (options = {}) => {
  const { format = 'dev', tokens = {}, ...config } = options

  // custom tokens
  for (const [k, v] of Object.entries(tokens)) morgan.token(k, v)

  return morgan(format, {
    skip: function (req, res) { return res.statusCode < 400 }
  })
}