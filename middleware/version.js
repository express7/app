'use strict'

const debug = require('debug')('app:version:middleware')
const semver = require('semver')
const createError = require('http-errors')

// opts:
//   errorConstructor: (Optional) function to construct errors with (default: Error)
//   getVersion: (Optional) Function to turn an express request object into a string representing the version
//     ex: Can be used to parse the version from wherever in the request you want (as opposed to the default header)
//         `(req) => req.body.version`
//   header: (Optional) header field specifying the name of the header to check for the version (default: 'accept-version')
//
//   Cannot set both header and getVersion opts

// checkHeader :: String -> Express.Request -> String
// const checkHeader = (header) => (req) => req.get(header)
const check = (header, param, dfault) => (req) => req.get(header) || req.query[param] || dfault

// version :: Object -> String -> (Express Middleware)
module.exports = (opts = {}) => {
  // const ErrCons = opts.errorConstructor || Error
  // const getVersion = opts.getVersion || checkHeader(opts.header || 'accept-version')
  const getVersion = opts.getVersion || check(opts.header || 'accept-version', opts.header || 'api-version', opts.default)

  // Most likely the module was required but not initialzed - so when it's used as a middleware it's not actually a middleware. Throw immediatley to prevent runtime errors.
  if (typeof opts === 'string') { throw new Error('express-semver-routing expects config parameter to be an object, string provided. Make sure you are calling the module as a function before using it as a middleware. Ex: const version = require("express-semver-routing")()') }

  // Ambiguous fields set - throw since it's a programmer error
  if (opts.getVersion && opts.header) { throw new Error('Ambiguous arguments provided to express-semver-routing module. Cannot set both "getVersion" and "header" fields.') }

  return (svRange) => {
    // Invalid range in route definition - throw since it's a programmer error
    if (!semver.validRange(svRange)) { throw new Error('Invalid semver value provided: ' + svRange) }

    return function version (req, res, next) {
      const ver = getVersion(req)

      // Invalid versions specified by incoming request - pass error to client
      if (!ver) { return next(createError(500, 'No API version specified on request')) }
      if (!semver.valid(ver)) { return next(createError(500, 'Invalid API Version specified. Semver format expected (ex: 1.0.0)')) }

      return semver.satisfies(ver, svRange)
        ? next() // Continue processing existing route
        : next('route') // Exits current route definition
    }
  }
}
