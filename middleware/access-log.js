'use strict'

const debug = require('debug')('app:middleware:access-log')
const morgan = require('morgan')
const Model = require('app/model/log')

const dformat = {
  timeStamp: () => ':date[iso]',
  ipAddress: () => ':remote-addr',
  status: () => ':status',
  userId: () => ':userid',
  service: () => 'api',
  protocol: () => 'HTTP/:http-version',
  method: () => ':method',
  resource: () => ':url',
  // byteSent: () => ':req[content-length]',
  // byteRecv: () => ':res[content-length]',
  duration: () => ':response-time[3] ms'
}

const dtokens = {
  userid (req, res) {
    const { user: { username, userId = username, id = userId } = {} } = req

    return id
  }
}

module.exports = (options = {}) => {
  // create a rotating write stream
  const { format = dformat, tokens = {}, logEnable = true, ...rest } = options

  // custom tokens
  for (const [k, v] of Object.entries({ ...dtokens, ...tokens })) morgan.token(k, v)

  const log = typeof format !== 'string'
    ? Object.keys(Model.props).map(k => format[k] ? format[k]() : '-').join('\t')
    : format

  return logEnable
    ? morgan(log, { stream: require('app/logger') })
    : (req, res, next) => next()
} 
