'use strict'

const debug = require('debug')('app:router:restful')

const codes = {
  200: 'OK', // Response to a successful GET, PUT, PATCH or DELETE. Can also be used for a POST that doesn't result in a creation.
  201: 'CREATED', // Response to a POST that results in a creation. Should be combined with a Location header pointing to the location of the new resource
  204: 'NO_CONTENT', // Response to a successful request that won't be returning a body (like a DELETE request)
  304: 'NOT_MODIFIED', // Used when HTTP caching headers are in play
  400: 'BAD_REQUEST', // The request is malformed, such as if the body does not parse
  401: 'UNAUTHORIZED', // When no or invalid authentication details are provided. Also useful to trigger an auth popup if the API is used from a browser
  403: 'FORBIDDEN', // When authentication succeeded but authenticated user doesn't have access to the resource
  404: 'NOT_FOUND', // When a non-existent resource is requested
  405: 'METHOD_NOT_ALLOWED', // When an HTTP method is being requested that isn't allowed for the authenticated user
  409: 'CONFLICT', // Indicates a request conflict with the current state of the target resource
  410: 'GONE', // Indicates that the resource at this end point is no longer available. Useful as a blanket response for old API versions
  415: 'UNSUPPORTED_MEDIA_TYPE', // If incorrect content type was provided as part of the request
  422: 'UNPROCESSABLE_ENTITY', // Used for validation errors
  429: 'TOO_MANY_REQUESTS', // When a request is rejected due to rate limiting
  500: 'INTERNAL_ERROR' // Catch-all/Unknown error
}

module.exports = (options = {}) => {
  const statusCodes = { ...codes, ...options.status }

  return async (req, res, next) => {
    // intercept response end
    const resEnd = res.end

    res.end = function (content, encoding = 'utf-8') {
      if (content) {
        let json = Buffer.from(content, encoding).toString('ascii')

        try {
          json = JSON.parse(json)
        } catch (err) {
          json = { data: json }
        }

        if (json.status && json.message && json.data) return resEnd.apply(this, arguments)

        const {
          statusCode,
          locals: {
            message: errMessage = statusCodes[statusCode]
          } = {}
        } = res

        const {
          error,
          status = error || statusCode,
          message = errMessage,
          ...data
        } = json

        // set content
        arguments[0] = Buffer.from(JSON.stringify({ status, error, message, data }), encoding)

        res.set({ 'Content-Length': Buffer.byteLength(arguments[0], encoding) })
      }

      return resEnd.apply(this, arguments)
    }

    return next()
  }
}
