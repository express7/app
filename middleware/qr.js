'use strict'

const _ = require('app/helpers')
const Database = require('app/database')
const createError = require('http-errors')

module.exports = (operation, ...args) => {
  // set callback
  let cb = args.pop()

  if (typeof cb !== 'function') {
    if (cb) args.push(cb)
    cb = c => c
  }

  args = _.flatten(args)

  return async (req, res, next) => {
    try {
      // already db function
      if (typeof operation !== 'string') {
        const rows = await operation
        return res.json(rows)
      }

      // server and operation
      const [op, db] = operation.split(':').reverse()

      // if as table name
      if (!op.endsWith(')')) {
        const rows = await Database.use(db).from(op)
        return res.json(rows)
      }

      // if db function
      const reqp = req.all()

      const params = (Array.isArray(args) ? args : (typeof args === 'function' ? args() : [args]))
        .map(c => [undefined, '"', "'"].includes(c[0]) ? c.slice(1, -1) : reqp[c])

      const rows = await Database.use(db).fromFn(op, params)

      return res.json(cb(rows))
    } catch (err) {
      return next(createError(500, err))
    }
  }
}
