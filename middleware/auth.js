'use strict'

const debug = require('debug')('app:auth:middleware')
const jwt = require('../auth')
const createError = require('http-errors')

module.exports = (options = {}) => async (req, res, next) => {
  try {
    const { secretOrKey, statusField, activeState, ...opts } = { ...jwt.config, ...options }

    const token = jwt.extract(req)

    const decoded = await jwt.verify(token, secretOrKey, opts)
    const { payload = decoded } = decoded

    // check active
    if (payload[statusField] !== activeState) return next(createError(401, 'Profile is not Active'))

    // attach to request
    req.user = jwt.payload(payload, ['exp'])

    return next()
  } catch (err) {
    return next(createError(401, err))
  }
}
