const debug = require('debug')('app:middleware:role')
const _ = require('app/helpers')
const createError = require('http-errors')
// const { Role } = require('models')
const Role = require('app/model/role')

module.exports = (options = {}) => {
  debug('constructor %O', options)

  const makePattern = text => {
    // ! allowed flag
    let [allowed, val] = text[0] === '!' ? [, text.substr(1)] : [true, text]

    // from start 
    val = val[0] === '*' ? val.substr(1) : '^' + val

    // until end
    val = val.split('').reverse()[0] === '*' ? val.slice(0, -1) : val + '$'

    return ({ allowed, val }) 
  }

  const isMatched = (text, pattern, allowed) => {
    const regex = new RegExp(pattern, 'gi')
    const match = regex.test(text)
    return allowed ? match : !match
  }
  
  return async (req, res, next) => {
    const { user: { id, roles } = {} } = req

    if (!roles) return next(createError(403, 'forbidden'))

    const resources = _.chain(await Role.find({ name: roles }))
      .map(c => c.toJSON().resources)
      .flatten()
      .map(c => c === '*' ? '* *' : (c === '!' ? '! !' : c))
      .uniq()
      .value()
    
    // add user path
    if (id) resources.push(`* /user/${id}`)

    const patterns = resources.map(resource => {
      const [{ 
        val: method, 
        allowed: methodAllow
      }, { 
        val: path,
        allowed: pathAllow
      }] = resource.split(' ').map(c => makePattern(c))

      return ({
        resource,
        method,
        methodAllow,
        path,
        pathAllow
      })
    })

    const matches = patterns.filter(c => isMatched(req.path, c.path, c.pathAllow))
      .filter(c => isMatched(req.method, c.method, c.methodAllow))

    return !!matches.length
      ? next()
      : next(createError(403, 'forbidden'))
  }
}
